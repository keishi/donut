window.addEventListener('mousemove', userGestureHandler, {
  passive: true
});
window.addEventListener('keydown', userGestureHandler, {
  passive: true
});

let hadUserGesture = false;

function userGestureHandler(event) {
  if (!event.isTrusted || hadUserGesture)
    return;
  hadUserGesture = true;
  setTimeout(userGestureResetTimerFired, 10 * 1000);
}

function userGestureResetTimerFired() {
  hadUserGesture = false;
}

setInterval(reportTimerFired, 1000);
function reportTimerFired() {
  if (hadUserGesture)
    chrome.runtime.sendMessage({type: "usergesture"});
}

window.postMessage({
  "type": "notifyDonutExtensionId",
  "donutExtensionId": chrome.runtime.id
}, "*");

let showDetectedWatermarksEnabled = true;
let overlayCanvas = document.createElement('canvas');
let overlayCtx = overlayCanvas.getContext('2d');
overlayCanvas.style.pointerEvents = 'none';
overlayCanvas.style.position = 'fixed';
overlayCanvas.style.top = '0';
overlayCanvas.style.left = '0';
overlayCanvas.style.zIndex = '1000';
overlayCanvas.width = window.innerWidth;
overlayCanvas.height = window.innerHeight;
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    //console.log(sender.tab ?
    //            "from a content script:" + sender.tab.url :
    //            "from the extension", request);
    if (request['type'] === "foundWatermarks") {
      if (!showDetectedWatermarksEnabled) {
        return;
      }
      for (let watermark of request['watermarks']) {
        overlayCtx.strokeStyle = "#ff0000";
        overlayCtx.lineWidth = 4;
        overlayCtx.strokeRect(watermark.rect[0] - 4, watermark.rect[1] - 4, watermark.rect[2] + 8, watermark.rect[3] + 8);
      }
      setTimeout(() => {
        overlayCtx.clearRect(0, 0, overlayCanvas.width, overlayCanvas.height);
      }, 500);
      return;
    }
    if (request['type'] === 'enableShowDetectedWatermarks') {
      showDetectedWatermarksEnabled = true;
      //console.log("showDetectedWatermarksEnabled", showDetectedWatermarksEnabled);
      document.body.appendChild(overlayCanvas);
      return;
    }
  });

let visibleWatermarkElements = new Set();
window.addEventListener("load", () => {
  if (window != window.top) {
    return;
  }
  let observer = new IntersectionObserver((entries, observer) => {
    for (let entry of entries) {
      if (entry.isIntersecting) {
        visibleWatermarkElements.add(entry.target);
      } else {
        visibleWatermarkElements.delete(entry.target);
      }
    }
  }, {});
  
  var targets = document.querySelectorAll('[donut-watermark]');
  targets.forEach((target) => observer.observe(target));

  setInterval(() => {
    requestAnimationFrame(() => {
      let watermarks = [];
      for (let element of visibleWatermarkElements) {
        let id = element.getAttribute("donut-watermark");
        if (!id) {
          continue;
        }
        let boundingClientRect = element.getBoundingClientRect();
        watermarks.push({
          id: id,
          rect: [boundingClientRect.x, boundingClientRect.y, boundingClientRect.width, boundingClientRect.height]
        });
      }
      chrome.runtime.sendMessage({
        type: "reportElementWatermarks",
        watermarks: watermarks
      });
    });
  }, 500);
});
