const donationProductId = "6456363819466752";
const donationProductPrice = 100;
let payButtonInitiated = false;
console.log("wikipediaInject");

let timer = setInterval(timerFired, 100);

function timerFired() {
  if (payButtonInitiated) {
    return;
  }
  console.log("timerFired");
  let centralNotice = document.querySelector("#centralNotice");
  if (!centralNotice) {
    return;
  }
  initPayButton();
  clearInterval(timer);
}

function initPayButton() {
  payButtonInitiated = true;
  console.log("initPayButton");
  let style = document.createElement("style");
  style.innerHTML = `
  .banner {
    display: flex;
    flex-flow: row nowrap;
    height: 120px;
    border: 1px solid #666;
  }
  .banner[hidden] {
    display: none;
  }
  .banner img {
    width: 120px;
    height: 120px;
  }
  .banner-content {
    display: flex;
    align-items: center;
    flex-flow: column nowrap;
    flex: 1;
    font-size: 18px;
    padding: 8px;
  }
  .banner-content > div {
    flex: 1;
  }
  .pay-button-frame {
    border: none;
    width: 130px;
    height: 44px;
    margin: 
  }
  `;
  document.head.appendChild(style);

  let centralNotice = document.querySelector("#centralNotice");
  let banner = document.createElement("div");
  banner.classList.add("banner");
  banner.innerHTML = `
    <img src="https://www.meme-arsenal.com/memes/96cb533ad27037917e28a8a576b3f0db.jpg">
    <div class="banner-content">
      <div>If everyone reading this donated<br>we could end this fundraiser right now.</div>
      <iframe class="pay-button-frame" src="https://donut-pay.appspot.com/ui/pay_button.html?price=${donationProductPrice}&id=${donationProductId}&style=rt"></iframe>
    </div>
  `;
  centralNotice.innerHTML = "";
  centralNotice.appendChild(banner);

  window.addEventListener("message", (event => {
    let o = event.data;
    if (o['type'] === 'paymentComplete') {
      banner.hidden = true;
    }
  }), false);
}
