const donationProductId = "5523713684406272";
const donationProductPrice = 1000;
let payButtonInitiated = false;
console.log("wikipediaInject");

let timer = setInterval(timerFired, 100);

function timerFired() {
  if (payButtonInitiated) {
    return;
  }
  console.log("timerFired");
  let centralNotice = document.querySelector(".postActions");
  if (!centralNotice) {
    return;
  }
  initPayButton();
  clearInterval(timer);
}

function initPayButton() {
  payButtonInitiated = true;
  console.log("initPayButton");
  let style = document.createElement("style");
  style.innerHTML = `
  .banner {
    display: flex;
    flex-flow: column nowrap;
    width: 80px;
    text-align: center;
    align-items: center;
  }
  .banner .icon {
    background-image: url("data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9JzMwMHB4JyB3aWR0aD0nMzAwcHgnICBmaWxsPSIjMDAwMDAwIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgMTAwIDEwMCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMTAwIDEwMCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PGc+PHBhdGggZD0iTTU4LjI5Niw2MC44MTljMC40MjksMC4yODcsMC45MjMsMC40MzEsMS40MTgsMC40MzFzMC45OS0wLjE0NCwxLjQxOC0wLjQzMWMwLjg3My0wLjU4MiwyMS40MDEtMTQuMzc3LDI4Ljk2My0yOC4yMzQgICBjMS44NzEtMi44OTgsMi44NTgtNi4yNDUsMi44NTgtOS42ODhDOTIuOTUzLDEzLjAyNyw4NC45MjQsNSw3NS4wNTYsNWMtNy4yNTcsMC0xMi42MzEsNi4wMjEtMTUuMzQxLDkuMTY3ICAgQzU3LjAwNCwxMS4wMjEsNTEuNjMxLDUsNDQuMzc1LDVjLTkuODcsMC0xNy44OTksOC4wMjctMTcuODk5LDE3Ljg5N2MwLDMuNDQyLDAuOTg5LDYuNzg5LDIuODU4LDkuNjg4ICAgQzM2Ljg5NSw0Ni40NDIsNTcuNDIzLDYwLjIzNyw1OC4yOTYsNjAuODE5eiBNNDQuMzc1LDEwLjExMmM1LjExMiwwLDkuMzg4LDQuOTc1LDExLjY4Nyw3LjY0OCAgIGMxLjQyOCwxLjY2MiwyLjIxNSwyLjU3OSwzLjY1MywyLjU3OXMyLjIyNS0wLjkxNywzLjY1NS0yLjU3OWMyLjI5Ny0yLjY3NCw2LjU3My03LjY0OCwxMS42ODctNy42NDggICBjNy4wNDksMCwxMi43ODQsNS43MzYsMTIuNzg0LDEyLjc4NWMwLDIuNDgxLTAuNzIxLDQuODk2LTIuMDgzLDYuOTc5Yy0wLjAzOCwwLjA1OS0wLjA3NSwwLjEyLTAuMTA4LDAuMTgyICAgYy01Ljg0LDEwLjc4Ny0yMS4xODYsMjIuMTY5LTI1LjkzNCwyNS41MjNjLTQuNzQ4LTMuMzU0LTIwLjA5Mi0xNC43MzYtMjUuOTM0LTI1LjUyM2MtMC4wMzQtMC4wNjItMC4wNy0wLjEyMy0wLjEwOC0wLjE4MiAgIGMtMS4zNjItMi4wODMtMi4wODMtNC40OTctMi4wODMtNi45NzlDMzEuNTg5LDE1Ljg0OSwzNy4zMjQsMTAuMTEyLDQ0LjM3NSwxMC4xMTJ6Ij48L3BhdGg+PHBhdGggZD0iTTkwLjQ2MSw2MC4yMDFjLTQuNjc2LTIuNzM5LTguNTk3LDEuMzIzLTEwLjA3NCwyLjg1MWMtMy4xMTMsMy4yMjUtNi44NjMsNi40MTMtOC43NjEsNy45NzkgICBjLTAuODA2LTAuMzYxLTEuNjk1LTAuNTY2LTIuNjMxLTAuNTc1Yy01Ljc4My0wLjMxMS0xNC44OTctMS4zNTQtMTcuMTc4LTIuNzFjLTIuNDUxLTEuNDU3LTQuNDE0LTIuODgtNi4xNDUtNC4xMzQgICBjLTQuOTc1LTMuNjA2LTguOTAzLTYuNDUyLTE4LjY4Ni02LjQ1MmMtMi44MzYsMC01LjMyMiwwLjc4Ni03LjcyNywxLjU0OGMtMi40NjgsMC43ODItNC44LDEuNTIxLTcuNjEzLDEuNTIxICAgYy0zLjEwMywwLTUuNjI1LDIuNTIyLTUuNjI1LDUuNjI1VjgwLjE3YzAsMy4xMDMsMi41MjIsNS42MjUsNS42MjUsNS42MjVjNS4zNDgsMCwxMy43MDksMC4yNzUsMTUuNTU0LDAuODkxICAgYzMuMzI1LDEuMTA4LDExLjExOCw1LjcyOSwxMy44NCw3LjQxNWMwLjAzNSwwLjAyMSwwLjA3LDAuMDQzLDAuMTA1LDAuMDYyQzQyLjEzLDk0LjcxLDQzLjI0Nyw5NSw0NC4zNzUsOTVoMjYgICBjMi43NTIsMCw1LjgxMS0yLjU0Niw2Ljg3Mi00LjMwN2MzLjQwOS01LjY1NywxNS42MzMtMjEuMDUsMTUuNzU2LTIxLjIwNWMwLjExLTAuMTQsMC4yMDUtMC4yOSwwLjI4NC0wLjQ0OCAgIEM5NC44NzEsNjUuODcyLDkzLjY1NSw2Mi4wNzEsOTAuNDYxLDYwLjIwMXogTTcwLjM3NCw4OS44ODVoLTI2Yy0wLjI0NiwwLTAuNDg5LTAuMDU5LTAuNzA2LTAuMTcgICBjLTAuOTYxLTAuNTk1LTEwLjQyMS02LjQwNC0xNC44NTEtNy44OGMtMy4yOTQtMS4wOTgtMTQuODgzLTEuMTU0LTE3LjE3MS0xLjE1NGMtMC4yODMsMC0wLjUxMy0wLjIyOS0wLjUxMy0wLjUxMVY2NS44NTMgICBjMC0wLjI4NCwwLjIzLTAuNTEzLDAuNTEzLTAuNTEzYzMuNjAyLDAsNi41NTQtMC45MzUsOS4xNTUtMS43NThjMi4yMjItMC43MDMsNC4xNC0xLjMxLDYuMTg1LTEuMzEgICBjOC4xMjQsMCwxMC45NywyLjA2MiwxNS42ODYsNS40NzljMS44MTksMS4zMTgsMy44ODIsMi44MTIsNi41Myw0LjM4OWMxLjMzLDAuNzkxLDMuNTMzLDEuNDEyLDYuMDAzLDEuODk1ICAgYzUuMzA5LDEuMDM4LDExLjg0OCwxLjQzOCwxMy41NzksMS41M2MwLjA0NCwwLjAwMywwLjA4OSwwLjAwMywwLjEzNSwwLjAwM2MwLjg0NSwwLDEuNTM0LDAuNjg4LDEuNTM0LDEuNTM1ICAgYzAsMC44NDYtMC42ODgsMS41MzItMS41MzQsMS41MzJjLTAuMDQ1LDAtMC4wODksMC4wMDMtMC4xMzUsMC4wMDVsLTE5LjM0OCwxLjAxOGMtMS41NDEtMC4wNTYtNS4xNzctMi42MzEtNy41OTItNC4zMzkgICBjLTAuNjI1LTAuNDQ0LTEuMjM1LTAuODctMS44MzUtMS4yODRjLTEuMzA3LTAuOTA0LTIuNTU1LTEuNzE2LTMuNjk3LTIuMjg3Yy0xLjI2My0wLjYzNS0yLjc5OC0wLjEyLTMuNDI5LDEuMTQzICAgcy0wLjEyLDIuNzk5LDEuMTQzLDMuNDMxYzEuMzE3LDAuNjU3LDMuMDQsMS44NzgsNC44NjUsMy4xN2MzLjgzNSwyLjcxNiw3LjQ1OSw1LjI4MiwxMC41OTcsNS4yODIgICBjMC4wNDUsMCwwLjA5MS0wLjAwMiwwLjEzNS0wLjAwM2wxOS4zNzMtMS4wMjFjMy42MzEtMC4wNCw2LjU3My0zLjAwOCw2LjU3My02LjY0NmMwLTAuODIzLTAuMTQ5LTEuNjExLTAuNDI1LTIuMzQxICAgYzIuMDUxLTEuNjk5LDUuNzQ1LTQuODY2LDguOTI0LTguMTU2YzIuMTc4LTIuMjU2LDMuMDQ3LTIuNDQsMy44MTItMS45OTJjMC42NzgsMC4zOTgsMS4xNzUsMS4xNzUsMC45MTMsMS45NTQgICBjLTEuNjk5LDIuMTQ1LTEyLjU1OCwxNS44OTktMTUuOTI0LDIxLjQ4NkM3Mi40Miw4OC43OTUsNzAuOTIsODkuODI3LDcwLjM3NCw4OS44ODV6Ij48L3BhdGg+PC9nPjwvc3ZnPg==");
    background-size: 60% 60%;
    background-repeat: no-repeat;
    background-position: center center;
    width: 80px;
    height: 80px;
    border-radius: 40px;
    border: 1px solid #666;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
  }
  .pay-button-frame {
    border: none;
    width: 130px;
    height: 44px;
  }
  .banner .pay-button-frame {
    transition: opacity 0.3s;
    opacity: 0;
    position: absolute;
    top: 18px;
    left: -25px;
  }
  .banner:hover .pay-button-frame {
    opacity: 1;
  }
  `;
  document.head.appendChild(style);

  let postActions = document.querySelector(".postActions");
  let banner = document.createElement("div");
  banner.classList.add("banner");
  banner.innerHTML = `
    <div class="message">DONATE</div>
    <div class="icon">
      <iframe class="pay-button-frame" src="https://donut-pay.appspot.com/ui/pay_button.html?price=${donationProductPrice}&id=${donationProductId}&style=rt"></iframe>
    </div>
  `;
  postActions.innerHTML = "";
  postActions.appendChild(banner);

  window.addEventListener("message", (event => {
    let o = event.data;
    if (o['type'] === 'paymentComplete') {
      alert("Thanks!");
    }
  }), false);
}
