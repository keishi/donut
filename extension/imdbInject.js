
const claimProductId = "5089913129140224";
const claimProductPrice = 50;

let t = setInterval(() => {
  let target = findThankYouMessage();
  if (target) {
    clearInterval(t);
    target.insertAdjacentHTML(`afterend`, `
    <style>
    .claim-button-frame {
      border: none;
      width: 130px;
      height: 44px;
    }
    </style>
    <p>
    Here is a small token of our appreciation.
    <div>
    <iframe class="claim-button-frame" src="https://donut-pay.appspot.com/ui/claim_button.html?price=${claimProductPrice}&id=${claimProductId}"></iframe>
    <div>
    <p>`);
  }
}, 100);

function findThankYouMessage() {
  let paragraphs = document.querySelectorAll('p');
  for (let p of paragraphs) {
    if (p.textContent === "Thank you for contributing to IMDb!") {
      return p;
    }
  }
  return null;
}
findThankYouMessage();