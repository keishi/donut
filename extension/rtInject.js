const singleArticleProductId = "5065210960281600";
const singleArticleProductPrice = 100;
let payButtonInitiated = false;
console.log("rtInject");

let timer = setInterval(timerFired, 100);

window.addEventListener("load", () => {
  timerFired();
  clearInterval(timer);
});

function timerFired() {
  if (payButtonInitiated) {
    return;
  }
  let articleText = document.querySelector(".article__text");
  if (!articleText) {
    return;
  }
  initPayButton();
  clearInterval(timer);
}

function initPayButton() {
  payButtonInitiated = true;
  console.log("initPayButton");
  let style = document.createElement("style");
  style.innerHTML = `
  .paywall {
    position: relative;
  }
  .paywall .article__text {
    filter: blur(4px);
  }
  .pay-button-frame {
    border: none;
    width: 130px;
    height: 44px;
    margin: 
  }
  .paywall-message {
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    font-size: 16px;
    padding: 24px;
    background-color: rgba(0, 0, 0, 0.5);
    color: white;
  }
  .paywall-message div {
    margin: 8px 0;
  }
  .paywall.disabled .article__text {
    filter: none;
  }
  .paywall.disabled .paywall-message {
    display: none;
  }
  .refund-bar {
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    height: 54px;
    background-color: #77bd1d;
    color: white;
    font-size: 16px;
  }
  .refund-message {
    flex: 1;
    padding: 8px 16px;
  }
  .refund-button {
    -webkit-appearance: none;
    background-color: #77bd1d;
    border: 1px solid #666;
    border-radius: 3px;
    padding: 8px;
    margin: 8px 16px;
  }
  `;
  document.head.appendChild(style);

  let articleText = document.querySelector(".article__text");
  let paywall = document.createElement("div");
  paywall.classList.add("paywall");
  articleText.parentElement.insertBefore(paywall, articleText);
  paywall.appendChild(articleText);

  let paywallMessage = document.createElement("div");
  paywallMessage.classList.add("paywall-message");
  paywallMessage.innerHTML = `
    <div>TO READ THE FULL STORY</div>
    <iframe class="pay-button-frame" src="https://donut-pay.appspot.com/ui/pay_button.html?price=${singleArticleProductPrice}&id=${singleArticleProductId}&style=rt"></iframe>
  `;
  paywall.appendChild(paywallMessage);

  document.getElementById("offers").innerHTML = "";

  window.addEventListener("message", (event => {
    let o = event.data;
    if (o['type'] === 'paymentComplete') {
      paywall.classList.add("disabled");
      let notifications = document.createElement("paywall-notifications");
      notifications.innerHTML = `
      <div class="refund-bar">
        <div class="refund-message">Thank you for your purchase!</div>
        <button class="refund-button" onclick="document.querySelector('.refund-message').textContent = 'Your purchase will be refunded.';document.querySelector('.refund-button').display = 'none';">Request a refund</button>
      </div>
      `;
      document.getElementById("header").insertBefore(notifications, document.getElementById("header").firstChild);
    }
  }), false);
}
