const tip50ProductPrice = 50;
const tip50ProductId = "5977430841884672";
const tip100ProductPrice = 100;
const tip100ProductId = "6456363819466752";
const tip200ProductPrice = 200;
const tip200ProductId = "4804066144157696";
const tip400ProductPrice = 400;
const tip400ProductId = "5783168229572608";
const tip800ProductPrice = 800;
const tip800ProductId = "5853909293727744";
const tip1600ProductPrice = 1600;
const tip1600ProductId = "4628627266207744";

let payButtonInitiated = false;
console.log("youtubeInject");

let timer;
window.onload = () => {
  timer = setInterval(timerFired, 100);
};

function timerFired() {
  if (payButtonInitiated) {
    return;
  }
  console.log("timerFired");
  let subscribeButton = document.querySelector("#subscribe-button.ytd-video-secondary-info-renderer");
  if (!subscribeButton) {
    return;
  }
  initPayButton();
  clearInterval(timer);
}

function initPayButton() {
  payButtonInitiated = true;
  console.log("initPayButton");
  let style = document.createElement("style");
  style.innerHTML = `
  #subscribe-button.ytd-video-secondary-info-renderer {
    flex-flow: row nowrap !important;
    align-items: center;
  }
  #tipButton {
    border: 1px solid #6c9dde;
    padding: 10px 16px;
    font-size: 1.4rem;
    margin: 8px 4px;
    border-radius: 2px;
    color: rgba(17, 17, 17, 0.6);
    position: relative;
  }
  #dialog {
    width: 414px;
    display: flex;
    flex-flow: row wrap;
  }
  #dialog[hidden] {
    display: none;
  }
  #banner {
    display: flex;
    flex-flow: column nowrap;
    position: absolute;
    background-color: #6c9dde;
    top: -132px;
    left: -184px;
    width: 414px;
    height: 104px;
    z-index: 10000;
    text-align: center;
    align-items: center;
    justify-content: center;
    border-radius: 10px;
    padding: 4px;
  }
  #banner:before {
    content: "";
    position: absolute;
    bottom: -20px;
    left: 50%;
    display: block;
    width: 0; 
    height: 0; 
    margin-left: -20px;
    border-left: 20px solid transparent;
    border-right: 20px solid transparent;
    border-top: 20px solid #6c9dde;
  }
  .pay-button-frame {
    border: none;
    width: 130px;
    height: 44px;
    margin: 4px;
  }
  `;
  document.head.appendChild(style);

  let subscribeButton = document.querySelector("#subscribe-button.ytd-video-secondary-info-renderer");
  let tipButton = document.createElement("div");
  tipButton.id = 'tipButton';
  tipButton.textContent = 'TIP';
  subscribeButton.insertBefore(tipButton, subscribeButton.firstChild);

  let banner = document.createElement("div");
  banner.id = "banner";
  banner.innerHTML = `
    <div id="dialog">
      <iframe class="pay-button-frame" src="https://donut-pay.appspot.com/ui/pay_button.html?price=${tip50ProductPrice}&id=${tip50ProductId}&style=youtube"></iframe>
      <iframe class="pay-button-frame" src="https://donut-pay.appspot.com/ui/pay_button.html?price=${tip100ProductPrice}&id=${tip100ProductId}&style=youtube"></iframe>
      <iframe class="pay-button-frame" src="https://donut-pay.appspot.com/ui/pay_button.html?price=${tip200ProductPrice}&id=${tip200ProductId}&style=youtube"></iframe>
      <iframe class="pay-button-frame" src="https://donut-pay.appspot.com/ui/pay_button.html?price=${tip400ProductPrice}&id=${tip400ProductId}&style=youtube"></iframe>
      <iframe class="pay-button-frame" src="https://donut-pay.appspot.com/ui/pay_button.html?price=${tip800ProductPrice}&id=${tip800ProductId}&style=youtube"></iframe>
      <iframe class="pay-button-frame" src="https://donut-pay.appspot.com/ui/pay_button.html?price=${tip1600ProductPrice}&id=${tip1600ProductId}&style=youtube"></iframe>
    </div>
  `;
  banner.hidden = true;
  tipButton.appendChild(banner);

  let closeHandler = event => {
    if (hasAncestor(event.target, banner)) {
      return;
    }
    event.preventDefault();
    banner.hidden = true;
    document.body.removeEventListener("click", closeHandler);
  };

  tipButton.addEventListener("click", (event => {
    banner.hidden = false;
    setTimeout(() => {
      document.body.addEventListener("click", closeHandler, false);
    }, 0);
  }), false);

  window.addEventListener("message", (event => {
    let o = event.data;
    if (o['type'] === 'paymentComplete') {
      banner.hidden = true;
    }
  }), false);
}

function hasAncestor(node, target) {
  let ancestor = node;
  while (ancestor) {
    if (ancestor === target) {
      return true;
    }
    ancestor = ancestor.parentElement;
  }
  return false;
}
