const singleArticleProductId = "5314105523044352";
const singleArticleProductPrice = 50;
const refundProductId = "5089913129140224";
const refundProductPrice = 50;
let payButtonInitiated = false;
console.log("rtInject");

let timer = setInterval(timerFired, 100);

window.addEventListener("load", () => {
  timerFired();
  clearInterval(timer);
});

function timerFired() {
  if (payButtonInitiated) {
    return;
  }
  let articleBody = document.querySelector(`[itemprop="articleBody"]`);
  if (!articleBody) {
    return;
  }
  initPayButton();
  clearInterval(timer);
}

function initPayButton() {
  payButtonInitiated = true;
  console.log("initPayButton");
  let style = document.createElement("style");
  style.innerHTML = `
  .donut-paywall {
    position: relative;
  }
  .donut-paywall-content {
    filter: blur(4px);
  }
  .refund-button-frame {
    border: none;
    width: 130px;
    height: 44px;
  }
  .pay-button-frame {
    border: none;
    width: 130px;
    height: 44px;
  }
  .paywall-message {
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    font-size: 16px;
    padding: 24px 24px 80px 24px;
    background-color: rgba(0, 0, 0, 0.5);
    color: white;
  }
  .paywall-message div {
    margin: 8px 0;
  }
  .donut-paywall.disabled .donut-paywall-content {
    filter: none;
  }
  .donut-paywall.disabled .paywall-message {
    display: none;
  }
  .refund-bar {
    position: relative;
    transform: translateY(54px);
    transition: transform 1s;
    top: 0;
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    height: 54px;
    background-color: #77bd1d;
    color: white;
    font-size: 16px;
  }
  .show .refund-bar {
    transform: translateY(0);
  }
  .refund-message {
    flex: 1;
    padding: 8px 16px;
  }
  donut-paywall-notifications {
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    overflow: hidden;
  }
  .refund-button-frame {
    margin-right: 16px;
  }
  `;
  document.head.appendChild(style);

  let articleBody = document.querySelector(`[itemprop="articleBody"]`);
  let firstParagraph = articleBody.querySelector("p");
  let paywall = document.createElement("div");
  paywall.classList.add(`donut-paywall`);
  let paywallContent = document.createElement("div");
  paywallContent.classList.add(`donut-paywall-content`);
  paywall.appendChild(paywallContent);
  articleBody.insertBefore(paywall, firstParagraph.nextSibling);
  while(paywall.nextSibling) {
    paywallContent.appendChild(paywall.nextSibling);
  }

  let paywallMessage = document.createElement("div");
  paywallMessage.classList.add("paywall-message");
  paywallMessage.innerHTML = `
    <div>TO READ THE FULL STORY</div>
    <iframe class="pay-button-frame" src="https://donut-pay.appspot.com/ui/pay_button.html?price=${singleArticleProductPrice}&id=${singleArticleProductId}&style=rt"></iframe>
  `;
  paywall.appendChild(paywallMessage);

  window.addEventListener("message", (event => {
    let o = event.data;
    if (o['type'] === 'paymentComplete') {
      if (o['id'] === singleArticleProductId) {
        paywall.classList.add("disabled");
        let notifications = document.createElement("donut-paywall-notifications");
        notifications.innerHTML = `
        <div class="refund-bar">
          <div class="refund-message">Thank you for your purchase!</div>
          <iframe class="refund-button-frame" src="https://donut-pay.appspot.com/ui/claim_button.html?price=${refundProductPrice}&id=${refundProductId}&style=refund"></iframe>
        </div>
        `;
        document.body.appendChild(notifications);
        document.querySelector(".refund-button-frame").onload = () => {
          notifications.classList.add("show");
        };
      } else if (o['id'] === refundProductId) {
        document.querySelector(".refund-message").textContent = "Your purchase will be refunded.";
        document.querySelector(".refund-button-frame").style.display = "none";
      }
    }
  }), false);
}
