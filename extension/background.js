let hadUserGesture = false;
let elementWatermarks = [];
let imageWatermarks = [];

function getVisibleDomain() {
  return new Promise((resolve, reject) => {
    chrome.tabs.query({
      active: true,
      lastFocusedWindow: true
    }, function (tabs) {
      var tab = tabs[0];
      if (!tab) {
        return;
      }
      let match = /^(?:https?:)?(?:\/\/)?([^\/\?]+)/.exec(tab.url);
      resolve(match[1]);
    });
  });
}

function getWalletId() {
  return new Promise((resolve, reject) => {
    chrome.storage.local.get(['walletId'], function (result) {
      resolve(result.walletId);
    });
  });
}

const pingInterval = 1000;

setInterval(() => {
  if (hadUserGesture) {
    sendPing();
  }
}, pingInterval);

async function sendPing() {
  console.log("sendPing");
  hadUserGesture = false
  let domain = await getVisibleDomain();
  let walletId = await getWalletId();
  if (!walletId) {
    return;
  }
  let watermarks = [];
  for (let x of elementWatermarks) {
    watermarks.push(x['id']);
  }
  for (let x of imageWatermarks) {
    watermarks.push(x['id']);
  }
  fetch("https://donut-pay.appspot.com/gaia/ping", {
    method: "POST",
    body: new URLSearchParams(`toid=${walletId}&domain=${domain}&watermarks=${watermarks.join(",")}`)
  });
  let allWatermarks = imageWatermarks.concat(elementWatermarks);
  //console.log("foundWatermarks", allWatermarks, watermarks);
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {
      type: "foundWatermarks",
      watermarks: allWatermarks
    }, function (response) {
      //console.log(response);
    });
  });
  elementWatermarks = [];
  imageWatermarks = [];
}

chrome.runtime.onMessageExternal.addListener((request, sender, sendResponse) => {
  //console.log("onMessageExternal", request);
  if (request["type"] === "syncWalletId") {
    console.log("syncWalletId", request["walletId"]);
    return new Promise((resolve, reject) => {
      chrome.storage.local.set({ walletId: request["walletId"] }, () => {
        // This doesn't seem to be called.
      });
      sendResponse({ success: !!chrome.runtime.lastError });
      resolve();
    });
  }
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  //console.log("onMessage", request);
  if (request["type"] === "usergesture") {
    hadUserGesture = true;
    return;
  }
  if (request["type"] === "reportElementWatermarks") {
    elementWatermarks = request['watermarks'];
  }
});
/*
chrome.browserAction.onClicked.addListener((activeTab) => {
  var newURL = "https://donut-pay.appspot.com/ui";
  chrome.tabs.create({ url: newURL });
});
*/
function fetchImageData(src) {
  return new Promise((resolve, reject) => {
    let img = new Image();
    img.onload = () => {
      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');
      canvas.width = img.width;
      canvas.height = img.height;
      ctx.drawImage(img, 0, 0);
      let data = ctx.getImageData(0, 0, img.width, img.height);
      resolve(data);
    };
    img.src = src;
  });
}

function filterWatermark(imageData) {
  let output = new ImageData(imageData.width, imageData.height);
  for (let y = 0; y < imageData.height; y++) {
    for (let x = 0; x < imageData.width; x++) {
      let index = (y * imageData.width + x) * 4;
      let r = imageData.data[index];
      let g = imageData.data[index + 1];
      let b = imageData.data[index + 2];
      let yp = 0.257 * r + 0.504 * g + 0.098 * b + 16;
      let v = 255;
      if (yp < 80) {
        v = 0;
      }
      output.data[index] = v;
      output.data[index + 1] = v;
      output.data[index + 2] = v;
      output.data[index + 3] = 255;
    }
  }
  return output;
}

function imageDataToUrl(imgData) {
  let canvas = document.createElement("canvas");
  let ctx = canvas.getContext("2d");
  ctx.putImageData(imgData, 0, 0);
  return canvas.toDataURL("image/png");
}

setInterval(() => {
  console.time("captureVisibleTab");
  chrome.tabs.captureVisibleTab({ "format": "png" }, async (imgUrl) => {
    console.timeEnd("captureVisibleTab");
    if (!imgUrl) {
      if (chrome.runtime.lastError) {
        console.log(chrome.runtime.lastError);
      }
      return;
    }
    console.time("decodeImage");
    let imgData = await fetchImageData(imgUrl);
    console.timeEnd("decodeImage");
    console.time("filterWatermark");
    let filtered = filterWatermark(imgData);
    //console.log(imageDataToUrl(filtered));
    console.timeEnd("filterWatermark");
    const code = jsQR(filtered.data, filtered.width, filtered.height);
    if (code) {
      let watermarkId = parseInt(code.data, 10);
      console.log(code, code.data, watermarkId);
      imageWatermarks = [{
        id: watermarkId,
        rect: [
          code.location.topLeftCorner.x / window.devicePixelRatio,
          code.location.topLeftCorner.y / window.devicePixelRatio,
          (code.location.bottomRightCorner.x - code.location.topLeftCorner.x) / window.devicePixelRatio,
          (code.location.bottomRightCorner.y - code.location.topLeftCorner.y) / window.devicePixelRatio
        ]
      }];
      console.log(
        code.location.topLeftCorner.x / window.devicePixelRatio,
        code.location.topLeftCorner.y / window.devicePixelRatio,
        (code.location.bottomRightCorner.x - code.location.topLeftCorner.x) / window.devicePixelRatio,
        (code.location.bottomRightCorner.y - code.location.topLeftCorner.y) / window.devicePixelRatio);
    } else {
      imageWatermarks = [];
    }
    //console.log(imageWatermarks);
  });
}, 2000);

chrome.contextMenus.create({
  "id": "show-detected-watermarks",
  "title": "Show Detected Watermarks",
  "contexts": ["page"]
});
chrome.contextMenus.onClicked.addListener((info, tab) => {
  if (info.menuItemId === "show-detected-watermarks") {
    chrome.tabs.sendMessage(tab.id, {
      type: "enableShowDetectedWatermarks"
    }, function (response) {
      //console.log(response);
    });
  }
});