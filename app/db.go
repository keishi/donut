package main

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"gopkg.in/square/go-jose.v2"
)

type Wallet struct {
	Label         string
	PublicKeyData []byte
	PublicKey     jose.JSONWebKey `datastore:"-"`
	Balance       int64
}

func (w *Wallet) Load(ps []datastore.Property) error {
	if err := datastore.LoadStruct(w, ps); err != nil {
		return errors.WithStack(err)
	}
	var privkey jose.JSONWebKey
	err := privkey.UnmarshalJSON(w.PublicKeyData)
	if err != nil {
		panic("FosterWallet.Load: Invalid PrivateKeyData")
	}
	w.PublicKey = privkey
	return nil
}

func (w *Wallet) Save() ([]datastore.Property, error) {
	d, err := w.PublicKey.MarshalJSON()
	if err != nil {
		panic("FosterWallet.Save: Invalid PrivateKey")
	}
	w.PublicKeyData = d
	return datastore.SaveStruct(w)
}

func GetWalletForID(ctx context.Context, id string) (*Wallet, error) {
	k := NewWalletKey(ctx, id)
	return GetWalletForKey(ctx, k)
}

func GetWalletForKey(ctx context.Context, k *datastore.Key) (*Wallet, error) {
	w := new(Wallet)
	if err := datastore.Get(ctx, k, w); err != nil {
		return nil, errors.Wrapf(err, "GetWalletForKey id=%s", k.StringID())
	}
	return w, nil
}

func GenerateWalletId(pubkey jose.JSONWebKey) string {
	thumb, err := pubkey.Thumbprint(crypto.SHA256)
	if err != nil {
		panic("GenerateWalletId: " + err.Error())
	}
	return hex.EncodeToString(thumb)
}

func NewWalletKey(ctx context.Context, id string) *datastore.Key {
	return datastore.NewKey(ctx, "Wallet", id, 0, nil)
}

func InsertNewWallet(ctx context.Context, label string, pubkey jose.JSONWebKey) (*datastore.Key, *Wallet, error) {
	w := &Wallet{
		Label:     label,
		PublicKey: pubkey,
		Balance:   0,
	}
	k, err := w.Insert(ctx)
	return k, w, err
}

func (w *Wallet) Insert(ctx context.Context) (*datastore.Key, error) {
	// FIXME: check existance first
	id := GenerateWalletId(w.PublicKey)
	k := NewWalletKey(ctx, id)
	k, err := datastore.Put(ctx, k, w)
	return k, errors.WithStack(err)
}

func (w *Wallet) Put(ctx context.Context, k *datastore.Key) error {
	_, err := datastore.Put(ctx, k, w)
	return errors.WithStack(err)
}

type TransactionKind int

const (
	NormalTransaction TransactionKind = iota
	GaiaToUserTransaction
	GaiaToWatermarkTransaction
)

func (k TransactionKind) String() string {
	switch k {
	case NormalTransaction:
		return "NormalTransaction"
	case GaiaToUserTransaction:
		return "GaiaToUserTransaction"
	case GaiaToWatermarkTransaction:
		return "GaiaToWatermarkTransaction"
	}
	return ""
}

func (k TransactionKind) MarshalJSON() ([]byte, error) {
	return json.Marshal(k.String())
}

type Transaction struct {
	FromKey                    *datastore.Key
	ToKey                      *datastore.Key
	Amount                     int64
	Kind                       TransactionKind
	GaiaToUserTransaction      bool
	GaiaToWatermarkTransaction bool
	Timestamp                  time.Time
}

func (t *Transaction) Save() ([]datastore.Property, error) {
	if t.GaiaToUserTransaction != (t.Kind == GaiaToUserTransaction) {
		return nil, errors.New("Invalid GaiaToUserTransaction field in Transaction")
	}
	if t.GaiaToWatermarkTransaction != (t.Kind == GaiaToWatermarkTransaction) {
		return nil, errors.New("Invalid GaiaToWatermarkTransaction field in Transaction")
	}
	return datastore.SaveStruct(t)
}

type VerifyTransactionSignatureFunc func(fromWallet *Wallet) bool

func PutTransaction(ctx context.Context, fromWalletID string, toWalletID string, amount int64, kind TransactionKind, signature []byte) (k *datastore.Key, transaction *Transaction, fromWallet *Wallet, toWallet *Wallet, err error) {
	err = datastore.RunInTransaction(ctx, func(ctx context.Context) error {
		var err error
		k, transaction, fromWallet, toWallet, err = PutTransactionInternal(ctx, fromWalletID, toWalletID, amount, kind, func(fromWallet *Wallet) bool {
			message := []byte(fromWalletID + "/" + toWalletID + "/" + strconv.FormatInt(amount, 10))
			log.Errorf(ctx, "PutTransaction message %v", (fromWalletID + "/" + toWalletID + "/" + strconv.FormatInt(amount, 10)))
			hashed := sha256.Sum256(message)
			err = rsa.VerifyPKCS1v15(fromWallet.PublicKey.Key.(*rsa.PublicKey), crypto.SHA256, hashed[:], signature)
			return err == nil
		})
		return err
	}, &datastore.TransactionOptions{XG: true})
	return k, transaction, fromWallet, toWallet, err
}

func PutTransactionInternal(ctx context.Context, fromWalletID string, toWalletID string, amount int64, kind TransactionKind, verifyFunc VerifyTransactionSignatureFunc) (k *datastore.Key, transaction *Transaction, fromWallet *Wallet, toWallet *Wallet, err error) {
	k = datastore.NewIncompleteKey(ctx, "Transaction", nil)
	transaction = &Transaction{
		FromKey: NewWalletKey(ctx, fromWalletID),
		ToKey:   NewWalletKey(ctx, toWalletID),
		Amount:  amount,
		Kind:    kind,
		GaiaToUserTransaction:      kind == GaiaToUserTransaction,
		GaiaToWatermarkTransaction: kind == GaiaToWatermarkTransaction,
		Timestamp:                  time.Now(),
	}
	fromWallet, err = GetWalletForID(ctx, fromWalletID)
	if err != nil {
		return nil, nil, nil, nil, errors.Wrap(err, "From wallet not found")
	}
	toWallet, err = GetWalletForID(ctx, toWalletID)
	if err != nil {
		return nil, nil, nil, nil, errors.Wrap(err, "To wallet not found")
	}

	if !verifyFunc(fromWallet) {
		return nil, nil, nil, nil, errors.New("Signature verification failed")
	}

	if fromWallet.Balance < amount {
		return nil, nil, nil, nil, errors.New("Insufficient balance")
	}

	fromWallet.Balance -= amount
	toWallet.Balance += amount

	transaction.Timestamp = time.Now()
	k, err = datastore.Put(ctx, k, transaction)
	if err != nil {
		return nil, nil, nil, nil, errors.Wrap(err, "Put transaction failed")
	}

	fromWalletKey := NewWalletKey(ctx, fromWalletID)
	err = fromWallet.Put(ctx, fromWalletKey)
	if err != nil {
		return nil, nil, nil, nil, errors.Wrap(err, "Put from wallet failed")
	}

	toWalletKey := NewWalletKey(ctx, toWalletID)
	err = toWallet.Put(ctx, toWalletKey)
	if err != nil {
		return nil, nil, nil, nil, errors.Wrap(err, "Put to wallet failed")
	}

	return k, transaction, fromWallet, toWallet, err
}

func GetTransactionForKey(ctx context.Context, k *datastore.Key) (Transaction, error) {
	var transaction Transaction
	err := datastore.Get(ctx, k, &transaction)
	return transaction, err
}

type FosterWallet struct {
	WalletKey      *datastore.Key
	PrivateKeyData []byte
	PrivateKey     jose.JSONWebKey `datastore:"-"`
}

func (fw *FosterWallet) Load(ps []datastore.Property) error {
	if err := datastore.LoadStruct(fw, ps); err != nil {
		return errors.WithStack(err)
	}
	var privkey jose.JSONWebKey
	err := privkey.UnmarshalJSON(fw.PrivateKeyData)
	if err != nil {
		panic("FosterWallet.Load: Invalid PrivateKeyData")
	}
	fw.PrivateKey = privkey
	return nil
}

func (fw *FosterWallet) Save() ([]datastore.Property, error) {
	d, err := fw.PrivateKey.MarshalJSON()
	if err != nil {
		panic("FosterWallet.Save: Invalid PrivateKey")
	}
	fw.PrivateKeyData = d
	return datastore.SaveStruct(fw)
}

func GetFosterWalletForWalletKey(ctx context.Context, wk *datastore.Key) (*datastore.Key, *FosterWallet, error) {
	var fw FosterWallet
	q := datastore.NewQuery("FosterWallet").Filter("WalletKey =", wk).Limit(1)
	t := q.Run(ctx)
	k, err := t.Next(&fw)
	if err == datastore.Done {
		return nil, nil, errors.Wrap(datastore.ErrNoSuchEntity, "GetFosterWalletForWalletKey")
	}
	return k, &fw, errors.WithStack(err)
}

func PutFosterWallet(ctx context.Context, wk *datastore.Key, privkey jose.JSONWebKey) (*datastore.Key, *FosterWallet, error) {
	fw := &FosterWallet{
		WalletKey:  wk,
		PrivateKey: privkey,
	}
	k := datastore.NewIncompleteKey(ctx, "FosterWallet", nil)
	k, err := datastore.Put(ctx, k, fw)
	return k, fw, errors.WithStack(err)
}

func CreateFosterWallet(ctx context.Context, label string) (*datastore.Key, *FosterWallet, error) {
	rsakey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, errors.WithStack(err)
	}
	privkey := jose.JSONWebKey{
		Key:       rsakey,
		Algorithm: "RS256",
	}
	wk, _, err := InsertNewWallet(ctx, label, privkey.Public())
	if err != nil {
		return nil, nil, errors.WithStack(err)
	}
	fwk, fw, err := PutFosterWallet(ctx, wk, privkey)
	return fwk, fw, errors.WithStack(err)
}

type Product struct {
	MerchantKey *datastore.Key
	Price       int64
}

func NewProductKey(ctx context.Context, id int64) *datastore.Key {
	return datastore.NewKey(ctx, "Product", "", id, nil)
}

func GetProductForKey(ctx context.Context, k *datastore.Key) (*Product, error) {
	var product Product
	if err := datastore.Get(ctx, k, &product); err != nil {
		return nil, errors.WithStack(err)
	}
	return &product, nil
}

func GetProductForID(ctx context.Context, id int64) (*datastore.Key, *Product, error) {
	k := NewProductKey(ctx, id)
	product, err := GetProductForKey(ctx, k)
	return k, product, err
}

func (p *Product) Put(ctx context.Context) (*datastore.Key, error) {
	k := datastore.NewIncompleteKey(ctx, "Product", nil)
	k, err := datastore.Put(ctx, k, p)
	return k, errors.WithStack(err)
}

type Purchase struct {
	BuyerKey       *datastore.Key
	ProductKey     *datastore.Key
	TransactionKey *datastore.Key
	Timestamp      time.Time
}

func NewPurchaseKey(ctx context.Context, id int64) *datastore.Key {
	return datastore.NewKey(ctx, "Purchase", "", id, nil)
}

func GetPurchaseForKey(ctx context.Context, k *datastore.Key) (*Purchase, error) {
	var purchase Purchase
	if err := datastore.Get(ctx, k, &purchase); err != nil {
		return nil, errors.WithStack(err)
	}
	return &purchase, nil
}

func GetPurchaseForID(ctx context.Context, id int64) (*datastore.Key, *Purchase, error) {
	k := NewPurchaseKey(ctx, id)
	purchase, err := GetPurchaseForKey(ctx, k)
	return k, purchase, err
}

func (p *Purchase) Insert(ctx context.Context) (*datastore.Key, error) {
	k := datastore.NewIncompleteKey(ctx, "Purchase", nil)
	k, err := datastore.Put(ctx, k, p)
	return k, errors.WithStack(err)
}
