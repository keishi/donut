package main

import (
	"context"
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/hex"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"gopkg.in/square/go-jose.v2"
)

type WalletData struct {
	ID        string          `json:"id"`
	Label     string          `json:"label"`
	PublicKey jose.JSONWebKey `json:"publicKey"`
	Balance   int64           `json:"balance"`
}

func CreateWalletData(w *Wallet, k *datastore.Key) WalletData {
	return WalletData{
		ID:        k.StringID(),
		Label:     w.Label,
		PublicKey: w.PublicKey,
		Balance:   w.Balance,
	}
}

type TransactionData struct {
	ID        int64           `json:"id"`
	FromID    string          `json:"fromId"`
	FromLabel string          `json:"fromLabel"`
	ToID      string          `json:"toId"`
	ToLabel   string          `json:"toLabel"`
	Amount    int64           `json:"amount"`
	Kind      TransactionKind `json:"kind"`
	Timestamp time.Time       `json:"timestamp"`
}

func CreateTransactionData(t Transaction, k *datastore.Key, fromLabel string, toLabel string) TransactionData {
	return TransactionData{
		ID:        k.IntID(),
		FromID:    t.FromKey.StringID(),
		FromLabel: fromLabel,
		ToID:      t.ToKey.StringID(),
		ToLabel:   toLabel,
		Amount:    t.Amount,
		Kind:      t.Kind,
		Timestamp: t.Timestamp,
	}
}

type WatermarkData struct {
	ID      int64  `json:"id"`
	OwnerID string `json:"ownerId"`
}

func CreateWatermarkData(wm Watermark, k *datastore.Key) WatermarkData {
	return WatermarkData{
		ID:      k.IntID(),
		OwnerID: wm.OwnerKey.StringID(),
	}
}

type ProductData struct {
	ID         int64  `json:"id"`
	MerchantID string `json:merchantId`
	Price      int64  `json:price`
}

func CreateProductData(product *Product, k *datastore.Key) ProductData {
	return ProductData{
		ID:         k.IntID(),
		MerchantID: product.MerchantKey.StringID(),
		Price:      product.Price,
	}
}

type PurchaseData struct {
	ID         int64     `json:"id"`
	BuyerID    string    `json:"buyerId"`
	ProductID  int64     `json:"productId"`
	TractionID int64     `json:"transactionId"`
	Timestamp  time.Time `json:"timestamp"`
}

func CreatePurchaseData(purchase *Purchase, k *datastore.Key) PurchaseData {
	return PurchaseData{
		ID:         k.IntID(),
		BuyerID:    purchase.BuyerKey.StringID(),
		ProductID:  purchase.ProductKey.IntID(),
		TractionID: purchase.TransactionKey.IntID(),
		Timestamp:  purchase.Timestamp,
	}
}

func GetWalletHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	id := chi.URLParam(r, "walletId")
	wallet, err := GetWalletForID(ctx, id)
	if errors.Cause(err) == datastore.ErrNoSuchEntity {
		RenderHTTPError(ctx, w, err, http.StatusNotFound)
		return
	} else if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	k := NewWalletKey(ctx, id)
	RenderJSON(w, r, CreateWalletData(wallet, k))
}

func UpdateWalletHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	id := chi.URLParam(r, "walletId")
	label := r.FormValue("label")
	k := NewWalletKey(ctx, id)
	var wallet *Wallet
	err := datastore.RunInTransaction(ctx, func(ctx context.Context) error {
		var err error
		wallet, err = GetWalletForID(ctx, id)
		if err != nil {
			return err
		}
		wallet.Label = label
		return wallet.Put(ctx, k)
	}, nil)
	if errors.Cause(err) == datastore.ErrNoSuchEntity {
		RenderHTTPError(ctx, w, err, http.StatusNotFound)
		return
	} else if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	RenderJSON(w, r, CreateWalletData(wallet, k))
}

func GetWalletListHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	q := datastore.NewQuery("Wallet").Limit(1000)
	var wallets []Wallet
	keys, err := q.GetAll(ctx, &wallets)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
	}
	out := make([]WalletData, len(wallets))
	for i, w := range wallets {
		out[i] = CreateWalletData(&w, keys[i])
	}
	RenderJSON(w, r, out)
}

func CreateWalletHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	label := r.FormValue("label")
	pubkeyParam := r.FormValue("pubkey")
	var pubkey jose.JSONWebKey
	pubkey.UnmarshalJSON([]byte(pubkeyParam))
	k, wallet, err := InsertNewWallet(ctx, label, pubkey)
	if err != nil {
		RenderHTTPError(ctx, w, err, 500)
		return
	}

	RenderJSON(w, r, CreateWalletData(wallet, k))
}

func CreateTransactionHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	fromWalletID := r.FormValue("fromid")
	log.Debugf(ctx, "fromWalletID %s", fromWalletID)
	toWalletID := r.FormValue("toid")
	log.Debugf(ctx, "toWalletID %s", toWalletID)
	if fromWalletID == toWalletID {
		RenderHTTPError(ctx, w, errors.New("Transaction must be made between different wallets"), http.StatusBadRequest)
		return
	}
	amount, err := strconv.ParseInt(r.FormValue("amount"), 10, 64)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusBadRequest)
		return
	}
	signature, err := hex.DecodeString(r.FormValue("signature"))
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusBadRequest)
		return
	}

	k, transaction, fromWallet, toWallet, err := PutTransaction(ctx, fromWalletID, toWalletID, amount, NormalTransaction, signature)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}

	RenderJSON(w, r, CreateTransactionData(*transaction, k, fromWallet.Label, toWallet.Label))
}

func GetTransactionListHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	uq := r.URL.Query()
	q := datastore.NewQuery("Transaction").Limit(1000)
	fromidParams, ok := uq["fromid"]
	if ok {
		fromid := fromidParams[0]
		q = q.Filter("FromKey =", NewWalletKey(ctx, fromid))
	}
	toidParams, ok := uq["toid"]
	if ok {
		toid := toidParams[0]
		q = q.Filter("ToKey =", NewWalletKey(ctx, toid))
	}
	_, ok = uq["excludeGaiaToUser"]
	if ok {
		log.Debugf(ctx, "excludeGaiaToUser")
		q = q.Filter("GaiaToUserTransaction =", false)
	}
	q = q.Order("-Timestamp")
	var transactions []Transaction
	keys, err := q.GetAll(ctx, &transactions)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	log.Debugf(ctx, "len(keys) %v", len(keys))
	log.Debugf(ctx, "len(transactions) %v", len(transactions))
	m := make(map[string]*Wallet)
	for _, t := range transactions {
		m[t.FromKey.StringID()] = nil
		m[t.ToKey.StringID()] = nil
	}
	walletKeys := make([]*datastore.Key, 0, len(m))
	for id := range m {
		walletKeys = append(walletKeys, NewWalletKey(ctx, id))
	}
	wallets := make([]Wallet, len(walletKeys))
	for i, _ := range wallets {
		wallets[i] = Wallet{}
	}
	err = datastore.GetMulti(ctx, walletKeys, wallets)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	for i, k := range walletKeys {
		m[k.StringID()] = &wallets[i]
	}
	out := make([]TransactionData, len(transactions))
	for i, t := range transactions {
		out[i] = CreateTransactionData(t, keys[i], m[t.FromKey.StringID()].Label, m[t.ToKey.StringID()].Label)
	}
	RenderJSON(w, r, out)
}

func CreateWatermarkHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	ownerID := r.FormValue("ownerid")
	_, err := GetWalletForID(ctx, ownerID)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	wm := &Watermark{
		OwnerKey: NewWalletKey(ctx, ownerID),
	}
	k := datastore.NewIncompleteKey(ctx, "Watermark", nil)
	k, err = datastore.Put(ctx, k, wm)
	if err != nil {
		RenderHTTPError(ctx, w, err, 500)
		return
	}

	RenderJSON(w, r, CreateWatermarkData(*wm, k))
}

func CreateProductHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	merchantID := r.FormValue("merchantId")
	_, err := GetWalletForID(ctx, merchantID)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	price, err := strconv.ParseInt(r.FormValue("price"), 10, 64)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusBadRequest)
		return
	}

	product := &Product{
		MerchantKey: NewWalletKey(ctx, merchantID),
		Price:       price,
	}
	k, err := product.Put(ctx)
	if err != nil {
		RenderHTTPError(ctx, w, err, 500)
		return
	}

	RenderJSON(w, r, CreateProductData(product, k))
}

func GetProductHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	id, err := strconv.ParseInt(r.FormValue("productId"), 10, 64)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusBadRequest)
		return
	}
	k, product, err := GetProductForID(ctx, id)
	if errors.Cause(err) == datastore.ErrNoSuchEntity {
		RenderHTTPError(ctx, w, err, http.StatusNotFound)
		return
	} else if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	RenderJSON(w, r, CreateProductData(product, k))
}

func CreatePurchaseHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	productID, err := strconv.ParseInt(r.FormValue("productId"), 10, 64)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusBadRequest)
		return
	}
	buyerID := r.FormValue("buyerId")
	signature, err := hex.DecodeString(r.FormValue("signature"))
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusBadRequest)
		return
	}

	var purchase *Purchase
	var purchaseKey *datastore.Key
	err = datastore.RunInTransaction(ctx, func(ctx context.Context) error {
		productKey, product, err := GetProductForID(ctx, productID)
		if err != nil {
			return err
		}

		fromWalletID := buyerID
		toWalletID := product.MerchantKey.StringID()
		amount := product.Price
		var transactionKey *datastore.Key
		var transaction *Transaction
		transactionKey, transaction, _, _, err = PutTransactionInternal(ctx, fromWalletID, toWalletID, amount, NormalTransaction, func(fromWallet *Wallet) bool {
			message := []byte(buyerID + "/" + strconv.FormatInt(productID, 10) + "/" + strconv.FormatInt(product.Price, 10))
			log.Debugf(ctx, "%s", buyerID+"/"+strconv.FormatInt(productID, 10)+"/"+strconv.FormatInt(product.Price, 10))
			hashed := sha256.Sum256(message)
			err = rsa.VerifyPKCS1v15(fromWallet.PublicKey.Key.(*rsa.PublicKey), crypto.SHA256, hashed[:], signature)
			return err == nil
		})
		if err != nil {
			return err
		}
		purchase = &Purchase{
			BuyerKey:       NewWalletKey(ctx, buyerID),
			ProductKey:     productKey,
			TransactionKey: transactionKey,
			Timestamp:      transaction.Timestamp,
		}
		purchaseKey, err = purchase.Insert(ctx)
		return err
	}, &datastore.TransactionOptions{XG: true})
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}

	RenderJSON(w, r, CreatePurchaseData(purchase, purchaseKey))
}

func GetWalletPurchaseListHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	q := datastore.NewQuery("Purchase").Limit(20)

	buyerID := chi.URLParam(r, "walletId")
	q = q.Filter("BuyerKey =", NewWalletKey(ctx, buyerID))

	uq := r.URL.Query()
	productIDParams, ok := uq["productId"]
	if ok {
		productID, err := strconv.ParseInt(productIDParams[0], 10, 64)
		if err != nil {
			RenderHTTPError(ctx, w, err, http.StatusBadRequest)
			return
		}
		q = q.Filter("ProductKey =", NewProductKey(ctx, productID))
	}

	q = q.Order("-Timestamp")

	var purchases []Purchase
	keys, err := q.GetAll(ctx, &purchases)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	data := make([]PurchaseData, len(purchases))
	for i, p := range purchases {
		data[i] = CreatePurchaseData(&p, keys[i])
	}

	RenderJSON(w, r, data)
}

func APIRoutes() *chi.Mux {
	router := chi.NewRouter()
	router.Get("/wallet", GetWalletListHandle)
	router.Post("/wallet", CreateWalletHandle)
	router.Get("/wallet/{walletId}", GetWalletHandle)
	router.Put("/wallet/{walletId}", UpdateWalletHandle)
	router.Get("/transaction", GetTransactionListHandle)
	//router.Get("/transaction/{transactionID}", GetTransaction)
	router.Post("/transaction", CreateTransactionHandle)
	router.Post("/watermark", CreateWatermarkHandle)
	router.Post("/product", CreateProductHandle)
	router.Post("/product/{productId}", GetProductHandle)
	router.Post("/purchase", CreatePurchaseHandle)
	router.Get("/wallet/{walletId}/purchase", GetWalletPurchaseListHandle)
	return router
}
