

let autoExtensionId = "";

window.addEventListener("message", event => {
  if (event.data['type'] === "notifyDonutExtensionId") {
    autoExtensionId = event.data['donutExtensionId'];
    console.log("autoExtensionId", autoExtensionId);
  }
})

async function syncWalletIdToExtension() {
  document.getElementById("extensionIdField").value = localStorage.getItem("extensionIdOverride") || "";
  document.getElementById("extensionIdField").placeholder = autoExtensionId || "bafloiiickjljbgghgdmpkadpmbelnpe";
  let walletKey = await loadWalletKey();
  if (!walletKey) {
    return;
  }
  let extensionId = localStorage.getItem("extensionIdOverride") || autoExtensionId;
  if (extensionId) {
    chrome.runtime.sendMessage(extensionId, { type: "syncWalletId", walletId: walletKey.id }, (response) => {
      if (!response || !response['success']) {
        console.error("syncWalletId failed", response);
      }
    });
  }
}

async function updateMyWallet() {
  let walletKey = await loadWalletKey();
  if (!walletKey) {
    showCreatingWalletOverlay("Creating Wallet");
    await createWallet(generateRandomLabel());
    walletKey = await loadWalletKey();
    console.assert(walletKey);
    hideCreatingWalletOverlay();
  }
  document.getElementById("myWalletProgressBar").hidden = false;
  let walletId = window.localStorage.getItem("walletid");
  {
    let resp = await fetch(`/api/wallet/${walletId}`);
    let walletData = await resp.json();
    document.getElementById("balanceValue").textContent = toDisplayValue(walletData["balance"]);
    if (document.getElementById("labelField")) {
      document.getElementById("labelField").value = walletData["label"];
    }
  }
  if (document.getElementById("myWalletTransactionList")) {
    let outTransactions;
    let inTransactions;
    {
      let resp = await fetch(`/api/transaction?fromid=${walletId}${getExcludeGaiaToUser() === "hide" ? "&excludeGaiaToUser=yes" : ""}`);
      outTransactions = await resp.json();
    }
    {
      let resp = await fetch(`/api/transaction?toid=${walletId}${getExcludeGaiaToUser() === "hide" ? "&excludeGaiaToUser=yes" : ""}`);
      inTransactions = await resp.json();
    }
    document.getElementById("myWalletTransactionList").innerHTML = "";
    let transactions = outTransactions.concat(inTransactions);
    let uniquify = new Map();
    transactions.forEach(t => {uniquify.set(t['id'], t);});
    transactions = Array.from(uniquify.values());
    transactions.sort((a, b) => -a['timestamp'].localeCompare(b['timestamp']));
    transactions.forEach(t => {
      let title = t["fromLabel"];
      let subtitle = moment(new Date(t["timestamp"])).fromNow();
      let amount = t["amount"];
      if (t["fromId"] === walletId) {
        title = t["toLabel"];
        amount = -amount;
      }
      if (t['kind'] === 'GaiaToUserTransaction') {
        title = "Gift to User";
      } else if (t['kind'] === 'GaiaToDomainTransaction') {
        title = "Gift to Website";
      } else if (t['kind'] === 'GaiaToWatermarkTransaction') {
        title = "Gift to Content Origin";
      }
      document.getElementById("myWalletTransactionList").appendChild(createTransactionElement(title, subtitle, amount));
    });
  }
  document.getElementById("myWalletProgressBar").hidden = true;
}

async function updateWalletViewer() {
  document.getElementById("walletViewerProgressBar").hidden = false;
  let pathName = location.pathname;
  if (pathName.charAt(pathName.length - 1) === "/") {
    pathName = pathName.substr(0, pathName.length - 1);
  }
  let parts = pathName.split("/");
  let walletId = parts.pop();
  {
    let resp = await fetch(`/api/wallet/${walletId}`);
    let walletData = await resp.json();
    document.getElementById("walletViewerBalanceValue").textContent = toDisplayValue(walletData["balance"]);
    document.getElementById("walletViewerTitle").value = walletData["label"];
  }
  let outTransactions;
  let inTransactions;
  {
    let resp = await fetch(`/api/transaction?fromid=${walletId}`);
    outTransactions = await resp.json();
  }
  {
    let resp = await fetch(`/api/transaction?toid=${walletId}`);
    inTransactions = await resp.json();
  }
  document.getElementById("walletViewerTransactionList").innerHTML = "";
  let transactions = outTransactions.concat(inTransactions);
  transactions.sort((a, b) => -a['timestamp'].localeCompare(b['timestamp']));
  transactions.forEach(t => {
    let title = t["fromLabel"];
    let subtitle = t["fromId"];
    let amount = t["amount"];
    if (t["fromId"] === walletId) {
      title = t["toLabel"];
      subtitle = t["toId"];
      amount = -amount;
    }
    document.getElementById("walletViewerTransactionList").appendChild(createTransactionElement(title, subtitle, amount));
  });
  document.getElementById("walletViewerProgressBar").hidden = true;
}

async function updateWalletList() {
  let form = document.getElementById("createTransactionForm");
  let resp = await fetch("/api/wallet");
  let data = await resp.json();
  let toWalletSelect = form["toid"];
  toWalletSelect.innerHTML = "";
  data.forEach(wallet => {
    toWalletSelect.appendChild(new Option(`${wallet['label']} (${wallet['id']})`, wallet['id']));
  });
}

function showSendPanel() {
  updateWalletList();
  document.getElementById("sendPanel").hidden = false;
}

function hideSendPanel() {
  document.getElementById("sendPanel").hidden = true;
  updateMyWallet();
}

function showSettingsPanel() {
  document.getElementById("settingsPanel").hidden = false;
}

function hideSettingsPanel() {
  document.getElementById("settingsPanel").hidden = true;
  updateMyWallet();
}

function showCreatingWalletOverlay(message) {
  document.getElementById("spinnerLabel").textContent = message;
  document.getElementById("progressOverlay").hidden = false;
}

function hideCreatingWalletOverlay() {
  document.getElementById("progressOverlay").hidden = true;
}

function buf2hex(buffer) { // buffer is an ArrayBuffer
  return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

let transactionElementTemplate = document.createElement('template');
transactionElementTemplate.innerHTML = `
<donut-transition>
  <donut-transition-title-area>
    <donut-transition-title>AAAAAAAA</donut-transition-title>
    <donut-transition-subtitle>BBBB</donut-transition-subtitle>
  </donut-transition-title-area>
  <donut-transition-amount><span id="balanceValue" class="donut-value">0000</span></donut-transition-amount>
</donut-transition>`;
function createTransactionElement(title, subtitle, amount) {
  var clone = document.importNode(transactionElementTemplate.content, true);
  clone.querySelector("donut-transition-title").textContent = title;
  clone.querySelector("donut-transition-subtitle").textContent = subtitle;
  clone.querySelector("donut-transition-amount .donut-value").textContent = toDisplayValue(amount);
  return clone;
}

function downloadJSONString(str, fileName) {
  var dataUrl = "data:text/json;charset=utf-8," + encodeURIComponent(str);
  var anchor = document.createElement('a');
  anchor.setAttribute("href", dataUrl);
  anchor.setAttribute("download", fileName);
  document.body.appendChild(anchor);
  anchor.click();
  anchor.remove();
}

document.addEventListener("DOMContentLoaded", (event) => {
  initValueDisplay();
  if (document.body.classList.contains('summary-app')) {
    return;
  }
  document.getElementById('sendFab').addEventListener('click', (event) => {
    showSendPanel();
  }, false);
  document.getElementById('closeSendPanelButton').addEventListener('click', (event) => {
    hideSendPanel();
  }, false);
  document.getElementById('showSettingsPanelButton').addEventListener('click', (event) => {
    showSettingsPanel();
  }, false);
  document.getElementById('closeSettingsPanelButton').addEventListener('click', (event) => {
    hideSettingsPanel();
  }, false);
  document.getElementById("sendButton").addEventListener('click', () => {
    document.getElementById("createTransactionSubmitButton").click();
  });
  document.getElementById("exportButton").addEventListener('click', () => {
    downloadJSONString(localStorage.getItem("walletKey"), "mywallet.json");
  });
  document.getElementById("importButton").addEventListener('click', () => {
    let input = document.createElement("input");
    input.type = "file";
    input.accept = ".json";
    input.addEventListener('change', event => {
      if (input.files.length === 0) {
        return;
      }
      let file = input.files[0];
      let reader = new FileReader();
      reader.addEventListener("load", async (event) => {
        let walletKeyData = reader.result;
        let walletKey;
        try {
          walletKey = await readWalletKeyData(walletKeyData);
        } catch (error) {
          input.remove();
          alert('Failed to read wallet file. Invalid JSON.');
          console.error(error);
          return;
        }
        localStorage.setItem("walletid", walletKey.id);
        localStorage.setItem("walletKey", walletKeyData);
        input.remove();
        syncWalletIdToExtension();
        updateMyWallet();
      });
      reader.addEventListener("error", (event) => {
        input.remove();
        alert('Failed to read wallet file.');
        console.error(reader.error);
      });
      reader.readAsText(file);
    });
    document.body.appendChild(input);
    input.click();
  });
  document.getElementById("labelField").addEventListener('change', async (event) => {
    let walletId = localStorage.getItem("walletid");
    let form = document.getElementById("updateLabelForm");
    let params = new URLSearchParams(new FormData(form));
    let resp = await fetch(`/api/wallet/${walletId}`, {
      method: 'put',
      body: params,
    });
    if (resp.status === 200) {
      let data = await resp.json();
      console.log("wallet id:", data["id"]);
    } else {
      alert("Error: " + (await resp.text()));
    }
  });
  document.getElementById("valueDisplaySelector").addEventListener('change', async (event) => {
    setValueDisplay(document.getElementById("valueDisplaySelector").value);
  });
  document.getElementById("valueSymbolSelector").addEventListener('change', async (event) => {
    setValueSymbol(document.getElementById("valueSymbolSelector").value);
  });
  initExcludeGaiaToUser();
  document.getElementById("excludeGaiaToUserSelector").addEventListener('change', async (event) => {
    setExcludeGaiaToUser(document.getElementById("excludeGaiaToUserSelector").value);
  });
  document.getElementById("extensionIdField").addEventListener('change', async (event) => {
    let extensionIdOverride = document.getElementById("extensionIdField").value;
    localStorage.setItem("extensionIdOverride", extensionIdOverride);
    syncWalletIdToExtension();
  });
  document.getElementById("createTransactionForm").addEventListener('submit', async (event) => {
    event.preventDefault();
    let form = document.getElementById("createTransactionForm");
    form['fromid'].value = localStorage.getItem("walletid");
    let message = `${form['fromid'].value}/${form['toid'].value}/${form['amount'].value}`;
    let messageData = (new TextEncoder()).encode(message);
    let walletKey = await loadWalletKey();
    let signature = await window.crypto.subtle.sign(
      {
        name: "RSASSA-PKCS1-v1_5",
      },
      walletKey.privateKey, //from generateKey or importKey above
      messageData //ArrayBuffer of data you want to sign
    );
    form['signature'].value = buf2hex(signature);
    let params = new URLSearchParams(new FormData(form));
    let resp = await fetch("/api/transaction", {
      method: 'post',
      body: params,
    });
    if (resp.status === 200) {
      let data = await resp.json();
      alert("Success! transaction id:", data["id"]);
    } else {
      alert("Error: " + (await resp.text()));
    }
    hideSendPanel();
  });
});

window.onload = function () {
  syncWalletIdToExtension();
  updateMyWallet();
};

/*
if ('serviceWorker' in navigator) {
  // Register a service worker
  const registratoin = await navigator.serviceWorker.register(
    // A service worker JS file is separate
    'service-worker.js'
  );
  // Check if Payment Handler is available
  if (!registration.paymentManager) return;

  registration.paymentManager.instruments.set(
    // Payment instrument key can be any string.
    "https://donut-pay.appspot.com",
    // Payment instrument detail
    {
      name: 'Payment Handler Example',
      method: 'https://donut-pay.appspot.com/pay'
    }
  )
}
*/

function generateRandomLabel() {
  const adjectives = ["adorable",
    "beautiful",
    "elegant",
    "fancy",
    "glamorous",
    "handsome",
    "long",
    "magnificent",
    "sparkling",
    "red",
    "orange",
    "yellow",
    "green",
    "blue",
    "purple",
    "gray",
    "black",
    "white",
    "brave",
    "calm",
    "delightful",
    "faithful",
    "gentle",
    "happy",
    "jolly",
    "kind",
    "lively",
    "nice",
    "obedient",
    "proudre",
    "silly",
    "thankful",
    "wonderful",
    "victorious",
    "witty",
    "zealous",
    "huge",
    "fast",
    "slow",
    "young",
    "swift"];

  const animals = ["alligator",
    "ant",
    "bear",
    "bee",
    "bird",
    "camel",
    "cat",
    "cheetah",
    "chicken",
    "chimpanzee",
    "cow",
    "crocodile",
    "deer",
    "dog",
    "dolphin",
    "duck",
    "eagle",
    "elephant",
    "fish",
    "fly",
    "fox",
    "frog",
    "giraffe",
    "goat",
    "goldfish",
    "hamster",
    "hippopotamus",
    "horse",
    "kangaroo",
    "kitten",
    "lion",
    "lobster",
    "monkey",
    "octopus",
    "owl",
    "panda",
    "pig",
    "puppy",
    "rabbit",
    "rat",
    "scorpion",
    "seal",
    "shark",
    "sheep",
    "snail",
    "snake",
    "spider",
    "squirrel",
    "tiger",
    "turtle",
    "wolf",
    "zebra"];
  return adjectives[Math.floor(Math.random() * adjectives.length)] + "-" + animals[Math.floor(Math.random() * animals.length)];
}

function getExcludeGaiaToUser() {
  return localStorage.getItem("excludeGaiaToUser") || "show";
}

function setExcludeGaiaToUser(excludeGaiaToUser) {
  localStorage.setItem("excludeGaiaToUser", excludeGaiaToUser);
  let excludeGaiaToUserSelector = document.getElementById("excludeGaiaToUserSelector");
  if (excludeGaiaToUserSelector) {
    excludeGaiaToUserSelector.value = excludeGaiaToUser;
  }
}

function initExcludeGaiaToUser() {
  setExcludeGaiaToUser(getExcludeGaiaToUser());
}
