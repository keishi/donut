async function readWalletKeyData(walletKeyData) {
  let o = JSON.parse(walletKeyData);
  let privateKey = await crypto.subtle.importKey("jwk", o["privateKey"],
    {
      name: "RSASSA-PKCS1-v1_5",
      hash: { name: "SHA-256" },
    },
    false,
    ["sign"]);
  let publicKey = await crypto.subtle.importKey("jwk", o["publicKey"],
    {
      name: "RSASSA-PKCS1-v1_5",
      hash: { name: "SHA-256" },
    },
    false,
    ["verify"]);
  return {
    id: o["id"],
    privateKey: privateKey,
    publicKey: publicKey
  };
}

async function loadWalletKey() {
  let walletKeyData = localStorage.getItem("walletKey");
  if (walletKeyData === null) {
    return undefined;
  }
  return await readWalletKeyData(walletKeyData);
}

async function createWallet(label) {
  let formData = new FormData();
  formData.set('label', label);
  let key = await window.crypto.subtle.generateKey(
    {
      name: "RSASSA-PKCS1-v1_5",
      modulusLength: 2048, //can be 1024, 2048, or 4096
      publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
      hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
    },
    true, //whether the key is extractable (i.e. can be used in exportKey)
    ["sign", "verify"] //can be any combination of "sign" and "verify"
  );
  let publicKeyData = await crypto.subtle.exportKey(
    "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
    key.publicKey //can be a publicKey or privateKey, as long as extractable was true
  );
  formData.set('pubkey', JSON.stringify(publicKeyData));
  let params = new URLSearchParams(formData);
  let resp = await fetch("/api/wallet", {
    method: 'post',
    body: params,
  });
  if (resp.status !== 200) {
    console.error(resp);
  }
  let data = await resp.json();
  window.localStorage.setItem("walletid", data['id']);
  let privateKeyData = await crypto.subtle.exportKey(
    "jwk", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
    key.privateKey //can be a publicKey or privateKey, as long as extractable was true
  );
  window.localStorage.setItem("walletKey", JSON.stringify({
    "id": data['id'],
    "privateKey": privateKeyData,
    "publicKey": publicKeyData,
  }));
}

let toDisplayValue = toCommaSeparatedString;

function toCommaSeparatedString(x) {
  let s = x.toString();
  if (Math.abs(x) < 1000) {
    return '0.' + ('000' + s).substr(s.length);
  }
  return s.substr(0, s.length - 3).replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "." + s.substr(s.length - 3);
}

function toMetricPrefixedString(x) {
  if (x < 1000) {
    return x.toString();
  }
  if (x < 1000000) {
    return (x / 1000).toFixed(3) + "K";
  }
  if (x < 1000000000) {
    return (x / 1000000).toFixed(3) + "M";
  }
  if (x < 1000000000000) {
    return (x / 1000000000).toFixed(3) + "G";
  }
  return (x / 1000000000000).toFixed(3) + "T";
}

function getValueDisplay() {
  return localStorage.getItem("valueDisplay") || "exact";
}

function setValueDisplay(valueDisplay) {
  localStorage.setItem("valueDisplay", valueDisplay);
  let valueDisplaySelector = document.getElementById("valueDisplaySelector");
  if (valueDisplaySelector) {
    valueDisplaySelector.value = valueDisplay;
  }
  switch (valueDisplay) {
    case "exact":
      toDisplayValue = toCommaSeparatedString;
      break;
    case "approx":
      toDisplayValue = toMetricPrefixedString;
      break;
    default:
      throw `Unknown valueDisplay: ${valueDisplay}`;
  }
}

function initValueDisplay() {
  setValueDisplay(getValueDisplay());
  initValueSymbol();
}

function getValueSymbol() {
  return localStorage.getItem("valueSymbol") || "coin";
}

function setValueSymbol(valueSymbol) {
  localStorage.setItem("valueSymbol", valueSymbol);
  let valueSymbolSelector = document.getElementById("valueSymbolSelector");
  if (valueSymbolSelector) {
    valueSymbolSelector.value = valueSymbol;
  }
  document.body.setAttribute('symbol', valueSymbol)
}

function initValueSymbol() {
  setValueSymbol(getValueSymbol());
}
