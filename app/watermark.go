package main

import (
	"context"

	"github.com/pkg/errors"
	"google.golang.org/appengine/datastore"
)

type Watermark struct {
	OwnerKey *datastore.Key
	Domain   string
}

func GetWatermarkForID(ctx context.Context, id int64) (Watermark, error) {
	var wm Watermark
	k := datastore.NewKey(ctx, "Watermark", "", id, nil)
	if err := datastore.Get(ctx, k, &wm); err != nil {
		return Watermark{}, errors.WithStack(err)
	}
	return wm, nil
}

func GetWatermarkForDomain(ctx context.Context, domain string) (*datastore.Key, *Watermark, error) {
	var wm Watermark
	q := datastore.NewQuery("Watermark").Filter("Domain =", domain).Limit(1)
	t := q.Run(ctx)
	k, err := t.Next(&wm)
	if err == datastore.Done {
		return nil, nil, errors.Wrap(datastore.ErrNoSuchEntity, "GetFosterWalletForWalletKey")
	}
	return k, &wm, errors.WithStack(err)
}

func CreateWatermarkForDomain(ctx context.Context, ownerID string, domain string) (*datastore.Key, *Watermark, error) {
	k, _, err := GetWatermarkForDomain(ctx, domain)
	if err == nil {
		return nil, nil, errors.New("Watermark for domain already exists: " + domain)
	}
	if errors.Cause(err) != datastore.ErrNoSuchEntity {
		return nil, nil, errors.WithStack(err)
	}
	wm := &Watermark{
		OwnerKey: NewWalletKey(ctx, ownerID),
		Domain:   domain,
	}
	k = datastore.NewIncompleteKey(ctx, "Watermark", nil)
	k, err = datastore.Put(ctx, k, wm)
	return k, wm, err
}
