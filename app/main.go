package main

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"google.golang.org/appengine"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	// if statement redirects all invalid URLs to the root homepage.
	// Ex: if URL is http://[YOUR_PROJECT_ID].appspot.com/FOO, it will be
	// redirected to http://[YOUR_PROJECT_ID].appspot.com.
	if r.URL.Path != "/" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	fmt.Fprintln(w, "Hello, Gopher Network!")
}

func main() {
	router := chi.NewRouter()
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "index.html")
	})

	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "favicon.ico")
	})

	uifs := http.FileServer(http.Dir("ui"))
	http.Handle("/ui/", http.StripPrefix("/ui/", uifs))

	testerfs := http.FileServer(http.Dir("tester"))
	http.Handle("/tester/", http.StripPrefix("/tester/", testerfs))

	router.Mount("/api", APIRoutes())
	router.Mount("/gaia", GaiaRoutes())
	http.Handle("/", router)
	appengine.Main()
}
