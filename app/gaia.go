package main

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	jose "gopkg.in/square/go-jose.v2"
)

const gaiaPublicKeyData = `{"alg":"RS256","e":"AQAB","ext":true,"key_ops":["verify"],"kty":"RSA","n":"2BjGs7odbfeFJoEyoMop4-78KEmi5nC9apID6rTooLcbRvVAx2iR4eAXM_YiVPzogq4xgJhGy0mCWX4ga2p77Ab8t42ubrdiEWPZFcgj7o1NBsw5Z2ZSRePtCuTje9loZbI9pf2XkDdEe-qw595a0Dg0_RRS04Fy1VOEi-VrWkCp1f3v733zGa8YeyBKiO7czi8bu8ntnbcvBd5YIUICvA-LCKSQBslK--WHKxfEDY_mjTq7OZCpR115eajJHUDLTeHxzptKp3r1DL_eVDeSN9UOMiK-AlwvOq9YPAR8vF0Ph-QKSsSuAIfMaP51X6n9kaoerOi8sfYY6v_j-TBJYw"}`
const gaiaPrivateKeyData = `{"alg":"RS256","d":"FYxu8Agh8Xp1DBsRDd3q9yA4g9nymmZpSlcSI0BJUZFrcRQkErPCQahjbUU2NOGysSlRUAtJ7_E-GXzNcHisr84d5ZMwGY6ZgwtrtSjWhcNlssUAVRJr0i1W0LcfWhUavHKy2WUOzTeS_C-x1q_3v_tYNAO9TfsCv561lm-uWKWar2g7ij7j3NM-SGMtWEDuc3PdnDPaRNbtUqBkusHaVbliMEKhmbhVBRX0SYpqLNa0gHb0FdbY9W7zafy9fCSCII6lf7CnffYt-PKU3ZUXXYywkLLyW05zPIkCUHQAukGj0GSsuZ5cR_ZSRWGH3HwjlQzy-Kvdus5T_Kj8tptM4Q","dp":"egtN8nKzTlzqSN44efOlqDpZC0LWYZYJBw9ZUxWIRplKCqbvPJmYT9F6Ne0_WZehgKdv1szhXO0ZRSVbwre46wqn4GRye_r99bhkVpzc8BdYBrIy2lDyITDEKUKA0XQWC1GalPA1dCR0fxPcPFstU4jR47KItxrGVJ8Rj7AKb6E","dq":"JTyIA1ITUsB1_gjIvp8yN8KoU1LhWq7AtjqGAuWTBOQDx4f_n2V4Lp7GXyTDRcM1uq7uOHXNz0ku5SC6_yNzvBI8D9r-GfJjXMFl2DL8Kb26ot2rj1QAWeySKIGZyUatT7cRNxgFdkBSY_yLI7jwgp6GdOhBU5UwpnH56nPHv2U","e":"AQAB","ext":true,"key_ops":["sign"],"kty":"RSA","n":"2BjGs7odbfeFJoEyoMop4-78KEmi5nC9apID6rTooLcbRvVAx2iR4eAXM_YiVPzogq4xgJhGy0mCWX4ga2p77Ab8t42ubrdiEWPZFcgj7o1NBsw5Z2ZSRePtCuTje9loZbI9pf2XkDdEe-qw595a0Dg0_RRS04Fy1VOEi-VrWkCp1f3v733zGa8YeyBKiO7czi8bu8ntnbcvBd5YIUICvA-LCKSQBslK--WHKxfEDY_mjTq7OZCpR115eajJHUDLTeHxzptKp3r1DL_eVDeSN9UOMiK-AlwvOq9YPAR8vF0Ph-QKSsSuAIfMaP51X6n9kaoerOi8sfYY6v_j-TBJYw","p":"_wbhgtd80jwY9YgapM0zoG8FKeWmCVuMGgZrOdaUDV3ZbHJSpkbPD6VQpg2b7068cQJUkLXcimP5yD0xgY8TxIIiQiKVXFJTimoDy6wFTA1a0wS0wRcQCldM9trzfI8JB_BxBa3r126EYBd4PkqR4r8xfk00Qu2fI5d5A9deP-E","q":"2Ovd9E4_jWXqk1ZOieW2tA_W_nrlOsHdEd5TxdPOij2jJaH0bSkWQckLjtb1XMM7KXnDRjllSXo_ONW5FziLb2kWk1ftLgXa7zlEQetBUzEddY_qc1CvFF1MtfKsAaYhASV6EIDPIJS-stWb-P46-OyADiuPOENz6JN21tndwcM","qi":"vBXghuc7N9u3R8_qjZbjXsMRaS3LmcGD8SsmymfvdjaMN8P2WGTCTqHwamPANjvP0RxD84HpMJXMwVGfc6kuax1ODO45V10dPh4QHQ3J2bUdFOF-D15i6Sw5SY0akimGjhSnPiTxDYMKvrqKUT4RQz3mK0SUl-i7KpApOgYLyNg"}`

var gaiaWalletKey *datastore.Key

func GaiaPublicKey() jose.JSONWebKey {
	var gaiaPublicKey jose.JSONWebKey
	err := gaiaPublicKey.UnmarshalJSON([]byte(gaiaPublicKeyData))
	if err != nil {
		panic(`GaiaPublicKey: ` + err.Error())
	}
	return gaiaPublicKey
}

func GaiaPrivateKey() jose.JSONWebKey {
	var gaiaPrivateKey jose.JSONWebKey
	err := gaiaPrivateKey.UnmarshalJSON([]byte(gaiaPrivateKeyData))
	if err != nil {
		panic(`GaiaPrivateKey: ` + err.Error())
	}
	return gaiaPrivateKey
}

const InitialGaiaBalance = 5000000000000000

func GaiaWalletID() string {
	return GenerateWalletId(GaiaPublicKey())
}

func CreateGaiaWallet(ctx context.Context) error {
	wallet := &Wallet{
		Label:     "$GAIA",
		PublicKey: GaiaPublicKey(),
		Balance:   InitialGaiaBalance,
	}
	k, err := wallet.Insert(ctx)
	if err != nil {
		return err
	}
	gaiaWalletKey = k

	return nil
}

func EnsureGaiaWallet(ctx context.Context) {
	if gaiaWalletKey != nil {
		return
	}
	err := datastore.RunInTransaction(ctx, func(ctx context.Context) error {
		id := GaiaWalletID()
		_, err := GetWalletForID(ctx, id)
		if err == nil {
			return nil
		}
		if errors.Cause(err) != datastore.ErrNoSuchEntity {
			return err
		}
		return CreateGaiaWallet(ctx)
	}, nil)
	if err != nil {
		log.Criticalf(ctx, "EnsureGaiaWallet failed: %v", err)
		panic("EnsureGaiaWallet failed: " + err.Error())
	}
}

func SignTransaction(fromWalletPublicKey jose.JSONWebKey, fromWalletID string, toWalletID string, amount int64) ([]byte, error) {
	message := []byte(fromWalletID + "/" + toWalletID + "/" + strconv.FormatInt(amount, 10))
	hashed := sha256.Sum256(message)
	return rsa.SignPKCS1v15(rand.Reader, fromWalletPublicKey.Key.(*rsa.PrivateKey), crypto.SHA256, hashed[:])
}

func MakeGaiaPayment(ctx context.Context, toWalletID string, amount int64, kind TransactionKind) (*datastore.Key, *Transaction, *Wallet, *Wallet, error) {
	signature, err := SignTransaction(GaiaPrivateKey(), GaiaWalletID(), toWalletID, amount)
	if err != nil {
		return nil, nil, nil, nil, err
	}
	return PutTransaction(ctx, GaiaWalletID(), toWalletID, amount, kind, signature)
}

func GiftHandler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	EnsureGaiaWallet(ctx)

	toWalletID := r.FormValue("toid")
	if toWalletID == "" {
		RenderHTTPError(ctx, w, errors.New("Empty toid"), http.StatusBadRequest)
		return
	}
	amount, err := strconv.ParseInt(r.FormValue("amount"), 10, 64)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusBadRequest)
		return
	}
	k, transaction, fromWallet, toWallet, err := MakeGaiaPayment(ctx, toWalletID, amount, NormalTransaction)
	log.Debugf(ctx, "MakeGaiaPayment to domain err %v", err)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	RenderJSON(w, r, CreateTransactionData(*transaction, k, fromWallet.Label, toWallet.Label))
}

func PingHandler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	EnsureGaiaWallet(ctx)

	toUserAmount := int64(2)
	toDomainAmount := int64(4)
	toWatermarkAmount := int64(4)

	toWalletID := r.FormValue("toid")
	if toWalletID == "" {
		RenderHTTPError(ctx, w, errors.New("Empty toid"), http.StatusBadRequest)
		return
	}
	domain := r.FormValue("domain")
	watermarks := r.FormValue("watermarks")

	// Payment to the user must come first to validate the user before paying the others.
	userTransactionKey, userTransaction, userFromWallet, userToWallet, err := MakeGaiaPayment(ctx, toWalletID, toUserAmount, GaiaToUserTransaction)
	log.Debugf(ctx, "MakeGaiaPayment to user err %v", err)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}

	if domain != "" {
		_, wm, err := GetWatermarkForDomain(ctx, domain)
		log.Debugf(ctx, "GetWatermarkForDomain domain %v err %v", domain, errors.Cause(err))
		if errors.Cause(err) == datastore.ErrNoSuchEntity {
			_, fw, err := CreateFosterWallet(ctx, domain)
			if err != nil {
				RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
				return
			}
			_, wm, err = CreateWatermarkForDomain(ctx, fw.WalletKey.StringID(), domain)
			if err != nil {
				RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
				return
			}
		} else if err != nil {
			RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
			return
		}
		_, _, _, _, err = MakeGaiaPayment(ctx, wm.OwnerKey.StringID(), toDomainAmount, GaiaToWatermarkTransaction)
		log.Debugf(ctx, "MakeGaiaPayment to domain err %v", err)
		if err != nil {
			RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
			return
		}
	}

	if watermarks != "" {
		parts := strings.Split(watermarks, ",")
		for _, p := range parts {
			wmID, err := strconv.ParseInt(p, 10, 64)
			if err != nil {
				continue
			}
			wm, err := GetWatermarkForID(ctx, wmID)
			if err != nil {
				log.Debugf(ctx, "GetWatermarkForID error: %v", err)
				continue
			}
			_, _, _, _, err = MakeGaiaPayment(ctx, wm.OwnerKey.StringID(), toWatermarkAmount, GaiaToWatermarkTransaction)
			log.Debugf(ctx, "MakeGaiaPayment to watermark err %v", err)
			if err != nil {
				RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
				return
			}
		}
	}

	RenderJSON(w, r, CreateTransactionData(*userTransaction, userTransactionKey, userFromWallet.Label, userToWallet.Label))
}

func GaiaRoutes() *chi.Mux {
	router := chi.NewRouter()
	router.Post("/ping", PingHandler)
	router.Post("/gift", GiftHandler)
	return router
}
