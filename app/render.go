package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
	"google.golang.org/appengine/log"
)

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func RenderHTTPError(ctx context.Context, w http.ResponseWriter, err error, code int) {
	buf := bytes.NewBufferString("")
	fmt.Fprintf(buf, "[%d] %s\n", code, err.Error())
	if err, ok := err.(stackTracer); ok {
		for _, f := range err.StackTrace() {
			fmt.Fprintf(buf, "%+s:%d\n", f)
		}
	}
	s := buf.String()

	log.Errorf(ctx, "%v", s)

	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	fmt.Fprintf(w, "Error %s\n", s)
}

func RenderJSON(w http.ResponseWriter, r *http.Request, v interface{}) {
	buf := &bytes.Buffer{}
	enc := json.NewEncoder(buf)
	enc.SetIndent("", "  ")
	enc.SetEscapeHTML(true)
	if err := enc.Encode(v); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(200)
	w.Write(buf.Bytes())
}
