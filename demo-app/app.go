package main

import (
	"context"
	"fmt"
	"html/template"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"google.golang.org/appengine"
	"google.golang.org/appengine/blobstore"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/image"
	"google.golang.org/appengine/log"
)

type Post struct {
	Text      string
	Image     appengine.BlobKey
	Timestamp time.Time
	Watermark string
}

func GetPostForKey(ctx context.Context, k *datastore.Key) (*Post, error) {
	var post Post
	if err := datastore.Get(ctx, k, &post); err != nil {
		return nil, errors.WithStack(err)
	}
	return &post, nil
}

func GetPostForID(ctx context.Context, id int64) (*datastore.Key, *Post, error) {
	k := datastore.NewKey(ctx, "Post", "", id, nil)
	post, err := GetPostForKey(ctx, k)
	return k, post, err
}

func (p *Post) Insert(ctx context.Context) (*datastore.Key, error) {
	k := datastore.NewIncompleteKey(ctx, "Post", nil)
	k, err := datastore.Put(ctx, k, p)
	return k, errors.WithStack(err)
}

func (p *Post) ToTemplateData(ctx context.Context, k *datastore.Key) (*PostTemplateData, error) {
	var imageURL string
	if p.Image != "" {
		u, err := image.ServingURL(ctx, p.Image, &image.ServingURLOptions{
			Secure: true,
			Size:   1600,
		})
		if err != nil {
			return nil, err
		}
		imageURL = u.String()
	}
	permalink := fmt.Sprintf("https://donut-pay-demo.appspot.com/post/%d", k.IntID())
	return &PostTemplateData{
		ID:        k.IntID(),
		Text:      p.Text,
		ImageURL:  imageURL,
		Timestamp: p.Timestamp,
		Watermark: p.Watermark,
		TweetURL:  fmt.Sprintf("https://twitter.com/intent/tweet?url=%s", url.QueryEscape(permalink)),
	}, nil
}

type PostTemplateData struct {
	ID        int64
	Text      string
	ImageURL  string
	Timestamp time.Time
	Watermark string
	TweetURL  string
}

type RootTemplateData struct {
	Title              string
	Posts              []*PostTemplateData
	UploadURL          string
	SummaryTitle       string
	SummaryDescription string
	SummaryImageURL    string
}

var rootTemplate = template.Must(template.New("root").Parse(rootTemplateHTML))

const rootTemplateHTML = `
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="twitter:card" content="summary_large_image">
	<meta property="twitter:title" content="{{.SummaryTitle}}">
	<meta property="twitter:description" content="{{.SummaryDescription}}">
	<meta property="twitter:image" content="{{.SummaryImageURL}}">
	<title>{{.Title}}</title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<link href="/static/style.css" rel="stylesheet">
	<script src="/static/main.js"></script>
</head>

<body>

	<div id="container">
		<h1><a href="/">Chirpr</a></h1>

		<div id="formSection">
			<div id="formSectionTabBar">
				<button class="formSectionTab selected" for="textForm">Text</button>
				<button class="formSectionTab" for="imageForm">Image</button>
			</div>

			<form id="textForm" class="selected" action="/post" method="POST">
				<textarea name="text"></textarea>
				<input name="watermark" type="hidden" value="">
				<input type="submit" value="Post">
			</form>

			<form id="imageForm" action="{{.UploadURL}}" method="POST" enctype="multipart/form-data">
				<label src=""></label><input type="file" name="file">
				<input type="submit" value="Post">
			</form>
		</div>

		{{range .Posts}}
		<chirpr-post>
			{{if .Text}}
			<p class="chirpr-post-content"{{if .Watermark}}donut-watermark="{{.Watermark}}"{{end}}>{{.Text}}</p>
			{{end}}
			{{if .ImageURL}}
			<img class="chirpr-post-content" src="{{.ImageURL}}">
			{{end}}
			<chirpr-post-timestamp><a href="/post/{{.ID}}">{{.Timestamp}}</a></chirpr-post-timestamp>
			<a class="twitter-share-button" href="{{.TweetURL}}" data-size="large">
Tweet</a>
		</chirpr-post>
		{{end}}
	</div>

	<script>window.twttr = (function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0],
			t = window.twttr || {};
		if (d.getElementById(id)) return t;
		js = d.createElement(s);
		js.id = id;
		js.src = "https://platform.twitter.com/widgets.js";
		fjs.parentNode.insertBefore(js, fjs);
	
		t._e = [];
		t.ready = function(f) {
			t._e.push(f);
		};
	
		return t;
	}(document, "script", "twitter-wjs"));</script>
</body>

</html>
`

func GetPostListHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	q := datastore.NewQuery("Post").Order("-Timestamp").Limit(20)
	var posts []Post
	keys, err := q.GetAll(ctx, &posts)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
	}
	postsData := make([]*PostTemplateData, len(posts))
	var summaryDescription string
	var summaryImageURL string
	for i, p := range posts {
		ptd, err := p.ToTemplateData(ctx, keys[i])
		if err != nil {
			RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		}
		if i == 0 {
			summaryDescription = ptd.Text
			summaryImageURL = ptd.ImageURL
		}
		postsData[i] = ptd
	}

	uploadURL, err := blobstore.UploadURL(ctx, "/upload", nil)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/html")
	err = rootTemplate.Execute(w, RootTemplateData{
		Title:              "Chirpr",
		Posts:              postsData,
		UploadURL:          uploadURL.String(),
		SummaryTitle:       "Chirpr",
		SummaryDescription: summaryDescription,
		SummaryImageURL:    summaryImageURL,
	})
	if err != nil {
		log.Errorf(ctx, "%v", err)
	}
}

func GetPostHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	id, err := strconv.ParseInt(chi.URLParam(r, "postID"), 10, 64)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusBadRequest)
		return
	}
	k, post, err := GetPostForID(ctx, id)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	pd, err := post.ToTemplateData(ctx, k)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}

	uploadURL, err := blobstore.UploadURL(ctx, "/upload", nil)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/html")
	err = rootTemplate.Execute(w, RootTemplateData{
		Title:              "Chirpr",
		Posts:              []*PostTemplateData{pd},
		UploadURL:          uploadURL.String(),
		SummaryTitle:       "Chirpr",
		SummaryDescription: pd.Text,
		SummaryImageURL:    pd.ImageURL,
	})
	if err != nil {
		log.Errorf(ctx, "%v", err)
	}
}

func CreatePostHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	post := &Post{
		Text:      r.FormValue("text"),
		Watermark: r.FormValue("watermark"),
		Timestamp: time.Now(),
	}
	k, err := post.Insert(ctx)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/post/"+strconv.FormatInt(k.IntID(), 10), http.StatusFound)
}

func UploadHandle(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	blobs, _, err := blobstore.ParseUpload(r)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	file := blobs["file"]
	if len(file) == 0 {
		RenderHTTPError(ctx, w, errors.New("Need file"), http.StatusBadRequest)
	}
	post := &Post{
		Image:     file[0].BlobKey,
		Timestamp: time.Now(),
	}
	k, err := post.Insert(ctx)
	if err != nil {
		RenderHTTPError(ctx, w, err, http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/post/"+strconv.FormatInt(k.IntID(), 10), http.StatusFound)
}

func main() {
	router := chi.NewRouter()
	router.Get("/", GetPostListHandle)
	router.Post("/upload", UploadHandle)
	router.Get("/post", GetPostListHandle)
	router.Get("/post/{postID}", GetPostHandle)
	router.Post("/post", CreatePostHandle)

	webflicksfs := http.FileServer(http.Dir("webflicks"))
	http.Handle("/webflicks/", http.StripPrefix("/webflicks/", webflicksfs))

	http.Handle("/", router)
	appengine.Main()
}
