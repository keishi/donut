document.addEventListener("DOMContentLoaded", () => {
  document.querySelectorAll(".formSectionTab").forEach((tab) => {
    tab.addEventListener("click", (event) => {
      document.querySelector(".formSectionTab.selected").classList.remove("selected");
      document.querySelector("#formSection form.selected").classList.remove("selected");
      event.target.classList.add("selected");
      document.getElementById(event.target.getAttribute("for")).classList.add("selected");
    });
  });
  document.getElementById("textForm").addEventListener("submit", (event) => {
    let form = event.target;
    event.preventDefault();
    let watermarkGenerator = document.createElement("iframe");
    watermarkGenerator.style.width = 0;
    watermarkGenerator.style.height = 0;
    document.body.appendChild(watermarkGenerator);
    watermarkGenerator.addEventListener("load", (event) => {
      console.log('watermarkGenerator load');
      window.addEventListener("message", async (event) => {
        console.log('watermarkGenerator message');
        let data = JSON.parse(event.data);
        form['watermark'].value = data['id'];
        form.submit();
      }, { once: true });
    }, { once: true });
    watermarkGenerator.src = "https://donut-pay.appspot.com/ui/watermark_generator.html?qr=yes";
  });
  document.getElementById("imageForm").addEventListener("submit", (event) => {
    event.preventDefault();
    let form = event.target;
    let fileInput = form['file'];
    let file = fileInput.files[0];
    if (!file.type.match(/^image\/(png|jpeg|gif)$/)) {
      alert("file must be an image");
      return;
    }
    let reader = new FileReader();
    reader.onload = function (evt) {
      let image = new Image();
      image.onload = function () {
        console.log('image load');
        let canvas = document.createElement("canvas");
        canvas.width = image.width;
        canvas.height = image.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0);
        let srcImageData = ctx.getImageData(0, 0, image.width, image.height);
        let watermarkGenerator = document.createElement("iframe");
        watermarkGenerator.style.width = 0;
        watermarkGenerator.style.height = 0;
        document.body.appendChild(watermarkGenerator);
        watermarkGenerator.addEventListener("load", (event) => {
          console.log('watermarkGenerator load');
          window.addEventListener("message", async (event) => {
            console.log('watermarkGenerator message');
            let data = JSON.parse(event.data);
            let qrImageData = await fetchImageData(data['qr']);
            let imageData = mixImageData(srcImageData, qrImageData);
            //showImageData(imageData);
            let blob = imageDataToBlob(imageData);
            let formData = new FormData();
            formData.append("file", blob);
            var request = new XMLHttpRequest();
            request.addEventListener("load", () => {
              window.location = request.responseURL;
            });
            request.open("POST", form.action);
            request.send(formData);
            console.log(request);
            watermarkGenerator.remove();
          }, { once: true });
        }, { once: true });
        watermarkGenerator.src = "https://donut-pay.appspot.com/ui/watermark_generator.html?qr=yes";
      }
      image.src = evt.target.result;
    }
    reader.readAsDataURL(file);
  });
});

function dataURLToBlob(url) {
  var binStr = atob(url.split(',')[1]),
    len = binStr.length,
    arr = new Uint8Array(len);
  for (var i = 0; i < len; i++) {
    arr[i] = binStr.charCodeAt(i);
  }
  return new Blob([arr], { type: 'image/png' });
}

function imageDataToBlob(imageData) {
  let canvas = document.createElement("canvas");
  canvas.width = imageData.width;
  canvas.height = imageData.height;
  canvas.getContext('2d').putImageData(imageData, 0, 0);
  let url = canvas.toDataURL("image/png");
  return dataURLToBlob(url);
}

function showImageData(imageData) {
  let canvas = document.createElement("canvas");
  canvas.width = imageData.width;
  canvas.height = imageData.height;
  canvas.getContext('2d').putImageData(imageData, 0, 0);
  document.body.appendChild(canvas);
}

function fetchImageData(src) {
  return new Promise((resolve, reject) => {
    let img = new Image();
    img.onload = () => {
      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');
      canvas.width = img.width;
      canvas.height = img.height;
      ctx.drawImage(img, 0, 0);
      let data = ctx.getImageData(0, 0, img.width, img.height);
      resolve(data);
    };
    img.src = src;
  });
}

function mixImageData(srcImageData, qrImageData) {
  const out = new ImageData(
    new Uint8ClampedArray(srcImageData.data),
    srcImageData.width,
    srcImageData.height
  );
  let offsetX = Math.floor((srcImageData.width - qrImageData.width) / 2);
  let offsetY = Math.floor((srcImageData.height - qrImageData.height) / 2);
  for (let y = 0; y < qrImageData.height; y++) {
    for (let x = 0; x < qrImageData.width; x++) {
      let srcIndex = ((y + offsetY) * srcImageData.width + x + offsetX) * 4;
      let qrIndex = (y * qrImageData.width + x) * 4;
      out.data[srcIndex++] = qrImageData.data[qrIndex++];
      out.data[srcIndex++] = qrImageData.data[qrIndex++];
      out.data[srcIndex++] = qrImageData.data[qrIndex++];
      out.data[srcIndex++] = qrImageData.data[qrIndex++];
    }
  }
  return out;
}
